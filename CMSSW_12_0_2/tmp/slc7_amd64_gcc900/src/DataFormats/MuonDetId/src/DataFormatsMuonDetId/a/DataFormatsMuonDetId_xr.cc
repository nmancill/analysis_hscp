// Do NOT change. Changes will be lost next time file is generated

#define R__DICTIONARY_FILENAME tmpdIslc7_amd64_gcc900dIsrcdIDataFormatsdIMuonDetIddIsrcdIDataFormatsMuonDetIddIadIDataFormatsMuonDetId_xr
#define R__NO_DEPRECATION

/*******************************************************************/
#include <stddef.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <assert.h>
#define G__DICTIONARY
#include "RConfig.h"
#include "TClass.h"
#include "TDictAttributeMap.h"
#include "TInterpreter.h"
#include "TROOT.h"
#include "TBuffer.h"
#include "TMemberInspector.h"
#include "TInterpreter.h"
#include "TVirtualMutex.h"
#include "TError.h"

#ifndef G__ROOT
#define G__ROOT
#endif

#include "RtypesImp.h"
#include "TIsAProxy.h"
#include "TFileMergeInfo.h"
#include <algorithm>
#include "TCollectionProxyInfo.h"
/*******************************************************************/

#include "TDataMember.h"

// The generated code does not explicitly qualifies STL entities
namespace std {} using namespace std;

#include "TBuffer.h"
#include "TVirtualObject.h"
#include <vector>
#include "TSchemaHelper.h"


// Header files passed as explicit arguments
#include "src/DataFormats/MuonDetId/src/classes.h"

// Header files passed via #pragma extra_include

namespace ROOT {
   static TClass *DTChamberId_Dictionary();
   static void DTChamberId_TClassManip(TClass*);
   static void *new_DTChamberId(void *p = 0);
   static void *newArray_DTChamberId(Long_t size, void *p);
   static void delete_DTChamberId(void *p);
   static void deleteArray_DTChamberId(void *p);
   static void destruct_DTChamberId(void *p);

   // Function generating the singleton type initializer
   static TGenericClassInfo *GenerateInitInstanceLocal(const ::DTChamberId*)
   {
      ::DTChamberId *ptr = 0;
      static ::TVirtualIsAProxy* isa_proxy = new ::TIsAProxy(typeid(::DTChamberId));
      static ::ROOT::TGenericClassInfo 
         instance("DTChamberId", 11, "DataFormats/MuonDetId/interface/DTChamberId.h", 14,
                  typeid(::DTChamberId), ::ROOT::Internal::DefineBehavior(ptr, ptr),
                  &DTChamberId_Dictionary, isa_proxy, 12,
                  sizeof(::DTChamberId) );
      instance.SetNew(&new_DTChamberId);
      instance.SetNewArray(&newArray_DTChamberId);
      instance.SetDelete(&delete_DTChamberId);
      instance.SetDeleteArray(&deleteArray_DTChamberId);
      instance.SetDestructor(&destruct_DTChamberId);
      return &instance;
   }
   TGenericClassInfo *GenerateInitInstance(const ::DTChamberId*)
   {
      return GenerateInitInstanceLocal((::DTChamberId*)0);
   }
   // Static variable to force the class initialization
   static ::ROOT::TGenericClassInfo *_R__UNIQUE_DICT_(Init) = GenerateInitInstanceLocal((const ::DTChamberId*)0x0); R__UseDummy(_R__UNIQUE_DICT_(Init));

   // Dictionary for non-ClassDef classes
   static TClass *DTChamberId_Dictionary() {
      TClass* theClass =::ROOT::GenerateInitInstanceLocal((const ::DTChamberId*)0x0)->GetClass();
      DTChamberId_TClassManip(theClass);
   return theClass;
   }

   static void DTChamberId_TClassManip(TClass* ){
   }

} // end of namespace ROOT

namespace ROOT {
   static TClass *DTSuperLayerId_Dictionary();
   static void DTSuperLayerId_TClassManip(TClass*);
   static void *new_DTSuperLayerId(void *p = 0);
   static void *newArray_DTSuperLayerId(Long_t size, void *p);
   static void delete_DTSuperLayerId(void *p);
   static void deleteArray_DTSuperLayerId(void *p);
   static void destruct_DTSuperLayerId(void *p);

   // Function generating the singleton type initializer
   static TGenericClassInfo *GenerateInitInstanceLocal(const ::DTSuperLayerId*)
   {
      ::DTSuperLayerId *ptr = 0;
      static ::TVirtualIsAProxy* isa_proxy = new ::TIsAProxy(typeid(::DTSuperLayerId));
      static ::ROOT::TGenericClassInfo 
         instance("DTSuperLayerId", 12, "DataFormats/MuonDetId/interface/DTSuperLayerId.h", 12,
                  typeid(::DTSuperLayerId), ::ROOT::Internal::DefineBehavior(ptr, ptr),
                  &DTSuperLayerId_Dictionary, isa_proxy, 12,
                  sizeof(::DTSuperLayerId) );
      instance.SetNew(&new_DTSuperLayerId);
      instance.SetNewArray(&newArray_DTSuperLayerId);
      instance.SetDelete(&delete_DTSuperLayerId);
      instance.SetDeleteArray(&deleteArray_DTSuperLayerId);
      instance.SetDestructor(&destruct_DTSuperLayerId);
      return &instance;
   }
   TGenericClassInfo *GenerateInitInstance(const ::DTSuperLayerId*)
   {
      return GenerateInitInstanceLocal((::DTSuperLayerId*)0);
   }
   // Static variable to force the class initialization
   static ::ROOT::TGenericClassInfo *_R__UNIQUE_DICT_(Init) = GenerateInitInstanceLocal((const ::DTSuperLayerId*)0x0); R__UseDummy(_R__UNIQUE_DICT_(Init));

   // Dictionary for non-ClassDef classes
   static TClass *DTSuperLayerId_Dictionary() {
      TClass* theClass =::ROOT::GenerateInitInstanceLocal((const ::DTSuperLayerId*)0x0)->GetClass();
      DTSuperLayerId_TClassManip(theClass);
   return theClass;
   }

   static void DTSuperLayerId_TClassManip(TClass* ){
   }

} // end of namespace ROOT

namespace ROOT {
   static TClass *DTLayerId_Dictionary();
   static void DTLayerId_TClassManip(TClass*);
   static void *new_DTLayerId(void *p = 0);
   static void *newArray_DTLayerId(Long_t size, void *p);
   static void delete_DTLayerId(void *p);
   static void deleteArray_DTLayerId(void *p);
   static void destruct_DTLayerId(void *p);

   // Function generating the singleton type initializer
   static TGenericClassInfo *GenerateInitInstanceLocal(const ::DTLayerId*)
   {
      ::DTLayerId *ptr = 0;
      static ::TVirtualIsAProxy* isa_proxy = new ::TIsAProxy(typeid(::DTLayerId));
      static ::ROOT::TGenericClassInfo 
         instance("DTLayerId", 12, "DataFormats/MuonDetId/interface/DTLayerId.h", 12,
                  typeid(::DTLayerId), ::ROOT::Internal::DefineBehavior(ptr, ptr),
                  &DTLayerId_Dictionary, isa_proxy, 12,
                  sizeof(::DTLayerId) );
      instance.SetNew(&new_DTLayerId);
      instance.SetNewArray(&newArray_DTLayerId);
      instance.SetDelete(&delete_DTLayerId);
      instance.SetDeleteArray(&deleteArray_DTLayerId);
      instance.SetDestructor(&destruct_DTLayerId);
      return &instance;
   }
   TGenericClassInfo *GenerateInitInstance(const ::DTLayerId*)
   {
      return GenerateInitInstanceLocal((::DTLayerId*)0);
   }
   // Static variable to force the class initialization
   static ::ROOT::TGenericClassInfo *_R__UNIQUE_DICT_(Init) = GenerateInitInstanceLocal((const ::DTLayerId*)0x0); R__UseDummy(_R__UNIQUE_DICT_(Init));

   // Dictionary for non-ClassDef classes
   static TClass *DTLayerId_Dictionary() {
      TClass* theClass =::ROOT::GenerateInitInstanceLocal((const ::DTLayerId*)0x0)->GetClass();
      DTLayerId_TClassManip(theClass);
   return theClass;
   }

   static void DTLayerId_TClassManip(TClass* ){
   }

} // end of namespace ROOT

namespace ROOT {
   static TClass *DTWireId_Dictionary();
   static void DTWireId_TClassManip(TClass*);
   static void *new_DTWireId(void *p = 0);
   static void *newArray_DTWireId(Long_t size, void *p);
   static void delete_DTWireId(void *p);
   static void deleteArray_DTWireId(void *p);
   static void destruct_DTWireId(void *p);

   // Function generating the singleton type initializer
   static TGenericClassInfo *GenerateInitInstanceLocal(const ::DTWireId*)
   {
      ::DTWireId *ptr = 0;
      static ::TVirtualIsAProxy* isa_proxy = new ::TIsAProxy(typeid(::DTWireId));
      static ::ROOT::TGenericClassInfo 
         instance("DTWireId", 12, "DataFormats/MuonDetId/interface/DTWireId.h", 12,
                  typeid(::DTWireId), ::ROOT::Internal::DefineBehavior(ptr, ptr),
                  &DTWireId_Dictionary, isa_proxy, 12,
                  sizeof(::DTWireId) );
      instance.SetNew(&new_DTWireId);
      instance.SetNewArray(&newArray_DTWireId);
      instance.SetDelete(&delete_DTWireId);
      instance.SetDeleteArray(&deleteArray_DTWireId);
      instance.SetDestructor(&destruct_DTWireId);
      return &instance;
   }
   TGenericClassInfo *GenerateInitInstance(const ::DTWireId*)
   {
      return GenerateInitInstanceLocal((::DTWireId*)0);
   }
   // Static variable to force the class initialization
   static ::ROOT::TGenericClassInfo *_R__UNIQUE_DICT_(Init) = GenerateInitInstanceLocal((const ::DTWireId*)0x0); R__UseDummy(_R__UNIQUE_DICT_(Init));

   // Dictionary for non-ClassDef classes
   static TClass *DTWireId_Dictionary() {
      TClass* theClass =::ROOT::GenerateInitInstanceLocal((const ::DTWireId*)0x0)->GetClass();
      DTWireId_TClassManip(theClass);
   return theClass;
   }

   static void DTWireId_TClassManip(TClass* ){
   }

} // end of namespace ROOT

namespace ROOT {
   static TClass *CSCDetId_Dictionary();
   static void CSCDetId_TClassManip(TClass*);
   static void *new_CSCDetId(void *p = 0);
   static void *newArray_CSCDetId(Long_t size, void *p);
   static void delete_CSCDetId(void *p);
   static void deleteArray_CSCDetId(void *p);
   static void destruct_CSCDetId(void *p);

   // Function generating the singleton type initializer
   static TGenericClassInfo *GenerateInitInstanceLocal(const ::CSCDetId*)
   {
      ::CSCDetId *ptr = 0;
      static ::TVirtualIsAProxy* isa_proxy = new ::TIsAProxy(typeid(::CSCDetId));
      static ::ROOT::TGenericClassInfo 
         instance("CSCDetId", 11, "DataFormats/MuonDetId/interface/CSCDetId.h", 26,
                  typeid(::CSCDetId), ::ROOT::Internal::DefineBehavior(ptr, ptr),
                  &CSCDetId_Dictionary, isa_proxy, 12,
                  sizeof(::CSCDetId) );
      instance.SetNew(&new_CSCDetId);
      instance.SetNewArray(&newArray_CSCDetId);
      instance.SetDelete(&delete_CSCDetId);
      instance.SetDeleteArray(&deleteArray_CSCDetId);
      instance.SetDestructor(&destruct_CSCDetId);
      return &instance;
   }
   TGenericClassInfo *GenerateInitInstance(const ::CSCDetId*)
   {
      return GenerateInitInstanceLocal((::CSCDetId*)0);
   }
   // Static variable to force the class initialization
   static ::ROOT::TGenericClassInfo *_R__UNIQUE_DICT_(Init) = GenerateInitInstanceLocal((const ::CSCDetId*)0x0); R__UseDummy(_R__UNIQUE_DICT_(Init));

   // Dictionary for non-ClassDef classes
   static TClass *CSCDetId_Dictionary() {
      TClass* theClass =::ROOT::GenerateInitInstanceLocal((const ::CSCDetId*)0x0)->GetClass();
      CSCDetId_TClassManip(theClass);
   return theClass;
   }

   static void CSCDetId_TClassManip(TClass* ){
   }

} // end of namespace ROOT

namespace ROOT {
   static TClass *RPCDetId_Dictionary();
   static void RPCDetId_TClassManip(TClass*);
   static void *new_RPCDetId(void *p = 0);
   static void *newArray_RPCDetId(Long_t size, void *p);
   static void delete_RPCDetId(void *p);
   static void deleteArray_RPCDetId(void *p);
   static void destruct_RPCDetId(void *p);

   // Function generating the singleton type initializer
   static TGenericClassInfo *GenerateInitInstanceLocal(const ::RPCDetId*)
   {
      ::RPCDetId *ptr = 0;
      static ::TVirtualIsAProxy* isa_proxy = new ::TIsAProxy(typeid(::RPCDetId));
      static ::ROOT::TGenericClassInfo 
         instance("RPCDetId", 11, "DataFormats/MuonDetId/interface/RPCDetId.h", 16,
                  typeid(::RPCDetId), ::ROOT::Internal::DefineBehavior(ptr, ptr),
                  &RPCDetId_Dictionary, isa_proxy, 12,
                  sizeof(::RPCDetId) );
      instance.SetNew(&new_RPCDetId);
      instance.SetNewArray(&newArray_RPCDetId);
      instance.SetDelete(&delete_RPCDetId);
      instance.SetDeleteArray(&deleteArray_RPCDetId);
      instance.SetDestructor(&destruct_RPCDetId);
      return &instance;
   }
   TGenericClassInfo *GenerateInitInstance(const ::RPCDetId*)
   {
      return GenerateInitInstanceLocal((::RPCDetId*)0);
   }
   // Static variable to force the class initialization
   static ::ROOT::TGenericClassInfo *_R__UNIQUE_DICT_(Init) = GenerateInitInstanceLocal((const ::RPCDetId*)0x0); R__UseDummy(_R__UNIQUE_DICT_(Init));

   // Dictionary for non-ClassDef classes
   static TClass *RPCDetId_Dictionary() {
      TClass* theClass =::ROOT::GenerateInitInstanceLocal((const ::RPCDetId*)0x0)->GetClass();
      RPCDetId_TClassManip(theClass);
   return theClass;
   }

   static void RPCDetId_TClassManip(TClass* ){
   }

} // end of namespace ROOT

namespace ROOT {
   static TClass *RPCCompDetId_Dictionary();
   static void RPCCompDetId_TClassManip(TClass*);
   static void *new_RPCCompDetId(void *p = 0);
   static void *newArray_RPCCompDetId(Long_t size, void *p);
   static void delete_RPCCompDetId(void *p);
   static void deleteArray_RPCCompDetId(void *p);
   static void destruct_RPCCompDetId(void *p);

   // Function generating the singleton type initializer
   static TGenericClassInfo *GenerateInitInstanceLocal(const ::RPCCompDetId*)
   {
      ::RPCCompDetId *ptr = 0;
      static ::TVirtualIsAProxy* isa_proxy = new ::TIsAProxy(typeid(::RPCCompDetId));
      static ::ROOT::TGenericClassInfo 
         instance("RPCCompDetId", 10, "DataFormats/MuonDetId/interface/RPCCompDetId.h", 21,
                  typeid(::RPCCompDetId), ::ROOT::Internal::DefineBehavior(ptr, ptr),
                  &RPCCompDetId_Dictionary, isa_proxy, 12,
                  sizeof(::RPCCompDetId) );
      instance.SetNew(&new_RPCCompDetId);
      instance.SetNewArray(&newArray_RPCCompDetId);
      instance.SetDelete(&delete_RPCCompDetId);
      instance.SetDeleteArray(&deleteArray_RPCCompDetId);
      instance.SetDestructor(&destruct_RPCCompDetId);
      return &instance;
   }
   TGenericClassInfo *GenerateInitInstance(const ::RPCCompDetId*)
   {
      return GenerateInitInstanceLocal((::RPCCompDetId*)0);
   }
   // Static variable to force the class initialization
   static ::ROOT::TGenericClassInfo *_R__UNIQUE_DICT_(Init) = GenerateInitInstanceLocal((const ::RPCCompDetId*)0x0); R__UseDummy(_R__UNIQUE_DICT_(Init));

   // Dictionary for non-ClassDef classes
   static TClass *RPCCompDetId_Dictionary() {
      TClass* theClass =::ROOT::GenerateInitInstanceLocal((const ::RPCCompDetId*)0x0)->GetClass();
      RPCCompDetId_TClassManip(theClass);
   return theClass;
   }

   static void RPCCompDetId_TClassManip(TClass* ){
   }

} // end of namespace ROOT

namespace ROOT {
   static TClass *DTBtiId_Dictionary();
   static void DTBtiId_TClassManip(TClass*);
   static void *new_DTBtiId(void *p = 0);
   static void *newArray_DTBtiId(Long_t size, void *p);
   static void delete_DTBtiId(void *p);
   static void deleteArray_DTBtiId(void *p);
   static void destruct_DTBtiId(void *p);

   // Function generating the singleton type initializer
   static TGenericClassInfo *GenerateInitInstanceLocal(const ::DTBtiId*)
   {
      ::DTBtiId *ptr = 0;
      static ::TVirtualIsAProxy* isa_proxy = new ::TIsAProxy(typeid(::DTBtiId));
      static ::ROOT::TGenericClassInfo 
         instance("DTBtiId", 10, "DataFormats/MuonDetId/interface/DTBtiId.h", 32,
                  typeid(::DTBtiId), ::ROOT::Internal::DefineBehavior(ptr, ptr),
                  &DTBtiId_Dictionary, isa_proxy, 12,
                  sizeof(::DTBtiId) );
      instance.SetNew(&new_DTBtiId);
      instance.SetNewArray(&newArray_DTBtiId);
      instance.SetDelete(&delete_DTBtiId);
      instance.SetDeleteArray(&deleteArray_DTBtiId);
      instance.SetDestructor(&destruct_DTBtiId);
      return &instance;
   }
   TGenericClassInfo *GenerateInitInstance(const ::DTBtiId*)
   {
      return GenerateInitInstanceLocal((::DTBtiId*)0);
   }
   // Static variable to force the class initialization
   static ::ROOT::TGenericClassInfo *_R__UNIQUE_DICT_(Init) = GenerateInitInstanceLocal((const ::DTBtiId*)0x0); R__UseDummy(_R__UNIQUE_DICT_(Init));

   // Dictionary for non-ClassDef classes
   static TClass *DTBtiId_Dictionary() {
      TClass* theClass =::ROOT::GenerateInitInstanceLocal((const ::DTBtiId*)0x0)->GetClass();
      DTBtiId_TClassManip(theClass);
   return theClass;
   }

   static void DTBtiId_TClassManip(TClass* ){
   }

} // end of namespace ROOT

namespace ROOT {
   static TClass *DTTracoId_Dictionary();
   static void DTTracoId_TClassManip(TClass*);
   static void *new_DTTracoId(void *p = 0);
   static void *newArray_DTTracoId(Long_t size, void *p);
   static void delete_DTTracoId(void *p);
   static void deleteArray_DTTracoId(void *p);
   static void destruct_DTTracoId(void *p);

   // Function generating the singleton type initializer
   static TGenericClassInfo *GenerateInitInstanceLocal(const ::DTTracoId*)
   {
      ::DTTracoId *ptr = 0;
      static ::TVirtualIsAProxy* isa_proxy = new ::TIsAProxy(typeid(::DTTracoId));
      static ::ROOT::TGenericClassInfo 
         instance("DTTracoId", 10, "DataFormats/MuonDetId/interface/DTTracoId.h", 34,
                  typeid(::DTTracoId), ::ROOT::Internal::DefineBehavior(ptr, ptr),
                  &DTTracoId_Dictionary, isa_proxy, 12,
                  sizeof(::DTTracoId) );
      instance.SetNew(&new_DTTracoId);
      instance.SetNewArray(&newArray_DTTracoId);
      instance.SetDelete(&delete_DTTracoId);
      instance.SetDeleteArray(&deleteArray_DTTracoId);
      instance.SetDestructor(&destruct_DTTracoId);
      return &instance;
   }
   TGenericClassInfo *GenerateInitInstance(const ::DTTracoId*)
   {
      return GenerateInitInstanceLocal((::DTTracoId*)0);
   }
   // Static variable to force the class initialization
   static ::ROOT::TGenericClassInfo *_R__UNIQUE_DICT_(Init) = GenerateInitInstanceLocal((const ::DTTracoId*)0x0); R__UseDummy(_R__UNIQUE_DICT_(Init));

   // Dictionary for non-ClassDef classes
   static TClass *DTTracoId_Dictionary() {
      TClass* theClass =::ROOT::GenerateInitInstanceLocal((const ::DTTracoId*)0x0)->GetClass();
      DTTracoId_TClassManip(theClass);
   return theClass;
   }

   static void DTTracoId_TClassManip(TClass* ){
   }

} // end of namespace ROOT

namespace ROOT {
   static TClass *DTSectCollId_Dictionary();
   static void DTSectCollId_TClassManip(TClass*);
   static void *new_DTSectCollId(void *p = 0);
   static void *newArray_DTSectCollId(Long_t size, void *p);
   static void delete_DTSectCollId(void *p);
   static void deleteArray_DTSectCollId(void *p);
   static void destruct_DTSectCollId(void *p);

   // Function generating the singleton type initializer
   static TGenericClassInfo *GenerateInitInstanceLocal(const ::DTSectCollId*)
   {
      ::DTSectCollId *ptr = 0;
      static ::TVirtualIsAProxy* isa_proxy = new ::TIsAProxy(typeid(::DTSectCollId));
      static ::ROOT::TGenericClassInfo 
         instance("DTSectCollId", 10, "DataFormats/MuonDetId/interface/DTSectCollId.h", 18,
                  typeid(::DTSectCollId), ::ROOT::Internal::DefineBehavior(ptr, ptr),
                  &DTSectCollId_Dictionary, isa_proxy, 12,
                  sizeof(::DTSectCollId) );
      instance.SetNew(&new_DTSectCollId);
      instance.SetNewArray(&newArray_DTSectCollId);
      instance.SetDelete(&delete_DTSectCollId);
      instance.SetDeleteArray(&deleteArray_DTSectCollId);
      instance.SetDestructor(&destruct_DTSectCollId);
      return &instance;
   }
   TGenericClassInfo *GenerateInitInstance(const ::DTSectCollId*)
   {
      return GenerateInitInstanceLocal((::DTSectCollId*)0);
   }
   // Static variable to force the class initialization
   static ::ROOT::TGenericClassInfo *_R__UNIQUE_DICT_(Init) = GenerateInitInstanceLocal((const ::DTSectCollId*)0x0); R__UseDummy(_R__UNIQUE_DICT_(Init));

   // Dictionary for non-ClassDef classes
   static TClass *DTSectCollId_Dictionary() {
      TClass* theClass =::ROOT::GenerateInitInstanceLocal((const ::DTSectCollId*)0x0)->GetClass();
      DTSectCollId_TClassManip(theClass);
   return theClass;
   }

   static void DTSectCollId_TClassManip(TClass* ){
   }

} // end of namespace ROOT

namespace ROOT {
   static TClass *GEMDetId_Dictionary();
   static void GEMDetId_TClassManip(TClass*);
   static void *new_GEMDetId(void *p = 0);
   static void *newArray_GEMDetId(Long_t size, void *p);
   static void delete_GEMDetId(void *p);
   static void deleteArray_GEMDetId(void *p);
   static void destruct_GEMDetId(void *p);

   // Schema evolution read functions
   static void read_GEMDetId_0( char* target, TVirtualObject *oldObj )
   {
      //--- Automatically generated variables ---
      GEMDetId* newObj = (GEMDetId*)target;
      // Supress warning message.
      (void)oldObj;

      (void)newObj;

      //--- User's code ---
     
      GEMDetId tmp(GEMDetId::v12Form(newObj->rawId()));
      *newObj=tmp;
    
   }

   // Function generating the singleton type initializer
   static TGenericClassInfo *GenerateInitInstanceLocal(const ::GEMDetId*)
   {
      ::GEMDetId *ptr = 0;
      static ::TVirtualIsAProxy* isa_proxy = new ::TIsAProxy(typeid(::GEMDetId));
      static ::ROOT::TGenericClassInfo 
         instance("GEMDetId", 12, "DataFormats/MuonDetId/interface/GEMDetId.h", 18,
                  typeid(::GEMDetId), ::ROOT::Internal::DefineBehavior(ptr, ptr),
                  &GEMDetId_Dictionary, isa_proxy, 12,
                  sizeof(::GEMDetId) );
      instance.SetNew(&new_GEMDetId);
      instance.SetNewArray(&newArray_GEMDetId);
      instance.SetDelete(&delete_GEMDetId);
      instance.SetDeleteArray(&deleteArray_GEMDetId);
      instance.SetDestructor(&destruct_GEMDetId);

      ::ROOT::Internal::TSchemaHelper* rule;

      // the io read rules
      std::vector<::ROOT::Internal::TSchemaHelper> readrules(1);
      rule = &readrules[0];
      rule->fSourceClass = "GEMDetId";
      rule->fTarget      = "";
      rule->fSource      = "";
      rule->fFunctionPtr = (void *)TFunc2void( read_GEMDetId_0);
      rule->fCode        = "\n      GEMDetId tmp(GEMDetId::v12Form(newObj->rawId()));\n      *newObj=tmp;\n    ";
      rule->fVersion     = "[-11]";
      instance.SetReadRules( readrules );
      return &instance;
   }
   TGenericClassInfo *GenerateInitInstance(const ::GEMDetId*)
   {
      return GenerateInitInstanceLocal((::GEMDetId*)0);
   }
   // Static variable to force the class initialization
   static ::ROOT::TGenericClassInfo *_R__UNIQUE_DICT_(Init) = GenerateInitInstanceLocal((const ::GEMDetId*)0x0); R__UseDummy(_R__UNIQUE_DICT_(Init));

   // Dictionary for non-ClassDef classes
   static TClass *GEMDetId_Dictionary() {
      TClass* theClass =::ROOT::GenerateInitInstanceLocal((const ::GEMDetId*)0x0)->GetClass();
      GEMDetId_TClassManip(theClass);
   return theClass;
   }

   static void GEMDetId_TClassManip(TClass* ){
   }

} // end of namespace ROOT

namespace ROOT {
   static TClass *ME0DetId_Dictionary();
   static void ME0DetId_TClassManip(TClass*);
   static void *new_ME0DetId(void *p = 0);
   static void *newArray_ME0DetId(Long_t size, void *p);
   static void delete_ME0DetId(void *p);
   static void deleteArray_ME0DetId(void *p);
   static void destruct_ME0DetId(void *p);

   // Function generating the singleton type initializer
   static TGenericClassInfo *GenerateInitInstanceLocal(const ::ME0DetId*)
   {
      ::ME0DetId *ptr = 0;
      static ::TVirtualIsAProxy* isa_proxy = new ::TIsAProxy(typeid(::ME0DetId));
      static ::ROOT::TGenericClassInfo 
         instance("ME0DetId", 11, "DataFormats/MuonDetId/interface/ME0DetId.h", 16,
                  typeid(::ME0DetId), ::ROOT::Internal::DefineBehavior(ptr, ptr),
                  &ME0DetId_Dictionary, isa_proxy, 12,
                  sizeof(::ME0DetId) );
      instance.SetNew(&new_ME0DetId);
      instance.SetNewArray(&newArray_ME0DetId);
      instance.SetDelete(&delete_ME0DetId);
      instance.SetDeleteArray(&deleteArray_ME0DetId);
      instance.SetDestructor(&destruct_ME0DetId);
      return &instance;
   }
   TGenericClassInfo *GenerateInitInstance(const ::ME0DetId*)
   {
      return GenerateInitInstanceLocal((::ME0DetId*)0);
   }
   // Static variable to force the class initialization
   static ::ROOT::TGenericClassInfo *_R__UNIQUE_DICT_(Init) = GenerateInitInstanceLocal((const ::ME0DetId*)0x0); R__UseDummy(_R__UNIQUE_DICT_(Init));

   // Dictionary for non-ClassDef classes
   static TClass *ME0DetId_Dictionary() {
      TClass* theClass =::ROOT::GenerateInitInstanceLocal((const ::ME0DetId*)0x0)->GetClass();
      ME0DetId_TClassManip(theClass);
   return theClass;
   }

   static void ME0DetId_TClassManip(TClass* ){
   }

} // end of namespace ROOT

namespace ROOT {
   // Wrappers around operator new
   static void *new_DTChamberId(void *p) {
      return  p ? new(p) ::DTChamberId : new ::DTChamberId;
   }
   static void *newArray_DTChamberId(Long_t nElements, void *p) {
      return p ? new(p) ::DTChamberId[nElements] : new ::DTChamberId[nElements];
   }
   // Wrapper around operator delete
   static void delete_DTChamberId(void *p) {
      delete ((::DTChamberId*)p);
   }
   static void deleteArray_DTChamberId(void *p) {
      delete [] ((::DTChamberId*)p);
   }
   static void destruct_DTChamberId(void *p) {
      typedef ::DTChamberId current_t;
      ((current_t*)p)->~current_t();
   }
} // end of namespace ROOT for class ::DTChamberId

namespace ROOT {
   // Wrappers around operator new
   static void *new_DTSuperLayerId(void *p) {
      return  p ? new(p) ::DTSuperLayerId : new ::DTSuperLayerId;
   }
   static void *newArray_DTSuperLayerId(Long_t nElements, void *p) {
      return p ? new(p) ::DTSuperLayerId[nElements] : new ::DTSuperLayerId[nElements];
   }
   // Wrapper around operator delete
   static void delete_DTSuperLayerId(void *p) {
      delete ((::DTSuperLayerId*)p);
   }
   static void deleteArray_DTSuperLayerId(void *p) {
      delete [] ((::DTSuperLayerId*)p);
   }
   static void destruct_DTSuperLayerId(void *p) {
      typedef ::DTSuperLayerId current_t;
      ((current_t*)p)->~current_t();
   }
} // end of namespace ROOT for class ::DTSuperLayerId

namespace ROOT {
   // Wrappers around operator new
   static void *new_DTLayerId(void *p) {
      return  p ? new(p) ::DTLayerId : new ::DTLayerId;
   }
   static void *newArray_DTLayerId(Long_t nElements, void *p) {
      return p ? new(p) ::DTLayerId[nElements] : new ::DTLayerId[nElements];
   }
   // Wrapper around operator delete
   static void delete_DTLayerId(void *p) {
      delete ((::DTLayerId*)p);
   }
   static void deleteArray_DTLayerId(void *p) {
      delete [] ((::DTLayerId*)p);
   }
   static void destruct_DTLayerId(void *p) {
      typedef ::DTLayerId current_t;
      ((current_t*)p)->~current_t();
   }
} // end of namespace ROOT for class ::DTLayerId

namespace ROOT {
   // Wrappers around operator new
   static void *new_DTWireId(void *p) {
      return  p ? new(p) ::DTWireId : new ::DTWireId;
   }
   static void *newArray_DTWireId(Long_t nElements, void *p) {
      return p ? new(p) ::DTWireId[nElements] : new ::DTWireId[nElements];
   }
   // Wrapper around operator delete
   static void delete_DTWireId(void *p) {
      delete ((::DTWireId*)p);
   }
   static void deleteArray_DTWireId(void *p) {
      delete [] ((::DTWireId*)p);
   }
   static void destruct_DTWireId(void *p) {
      typedef ::DTWireId current_t;
      ((current_t*)p)->~current_t();
   }
} // end of namespace ROOT for class ::DTWireId

namespace ROOT {
   // Wrappers around operator new
   static void *new_CSCDetId(void *p) {
      return  p ? new(p) ::CSCDetId : new ::CSCDetId;
   }
   static void *newArray_CSCDetId(Long_t nElements, void *p) {
      return p ? new(p) ::CSCDetId[nElements] : new ::CSCDetId[nElements];
   }
   // Wrapper around operator delete
   static void delete_CSCDetId(void *p) {
      delete ((::CSCDetId*)p);
   }
   static void deleteArray_CSCDetId(void *p) {
      delete [] ((::CSCDetId*)p);
   }
   static void destruct_CSCDetId(void *p) {
      typedef ::CSCDetId current_t;
      ((current_t*)p)->~current_t();
   }
} // end of namespace ROOT for class ::CSCDetId

namespace ROOT {
   // Wrappers around operator new
   static void *new_RPCDetId(void *p) {
      return  p ? new(p) ::RPCDetId : new ::RPCDetId;
   }
   static void *newArray_RPCDetId(Long_t nElements, void *p) {
      return p ? new(p) ::RPCDetId[nElements] : new ::RPCDetId[nElements];
   }
   // Wrapper around operator delete
   static void delete_RPCDetId(void *p) {
      delete ((::RPCDetId*)p);
   }
   static void deleteArray_RPCDetId(void *p) {
      delete [] ((::RPCDetId*)p);
   }
   static void destruct_RPCDetId(void *p) {
      typedef ::RPCDetId current_t;
      ((current_t*)p)->~current_t();
   }
} // end of namespace ROOT for class ::RPCDetId

namespace ROOT {
   // Wrappers around operator new
   static void *new_RPCCompDetId(void *p) {
      return  p ? new(p) ::RPCCompDetId : new ::RPCCompDetId;
   }
   static void *newArray_RPCCompDetId(Long_t nElements, void *p) {
      return p ? new(p) ::RPCCompDetId[nElements] : new ::RPCCompDetId[nElements];
   }
   // Wrapper around operator delete
   static void delete_RPCCompDetId(void *p) {
      delete ((::RPCCompDetId*)p);
   }
   static void deleteArray_RPCCompDetId(void *p) {
      delete [] ((::RPCCompDetId*)p);
   }
   static void destruct_RPCCompDetId(void *p) {
      typedef ::RPCCompDetId current_t;
      ((current_t*)p)->~current_t();
   }
} // end of namespace ROOT for class ::RPCCompDetId

namespace ROOT {
   // Wrappers around operator new
   static void *new_DTBtiId(void *p) {
      return  p ? new(p) ::DTBtiId : new ::DTBtiId;
   }
   static void *newArray_DTBtiId(Long_t nElements, void *p) {
      return p ? new(p) ::DTBtiId[nElements] : new ::DTBtiId[nElements];
   }
   // Wrapper around operator delete
   static void delete_DTBtiId(void *p) {
      delete ((::DTBtiId*)p);
   }
   static void deleteArray_DTBtiId(void *p) {
      delete [] ((::DTBtiId*)p);
   }
   static void destruct_DTBtiId(void *p) {
      typedef ::DTBtiId current_t;
      ((current_t*)p)->~current_t();
   }
} // end of namespace ROOT for class ::DTBtiId

namespace ROOT {
   // Wrappers around operator new
   static void *new_DTTracoId(void *p) {
      return  p ? new(p) ::DTTracoId : new ::DTTracoId;
   }
   static void *newArray_DTTracoId(Long_t nElements, void *p) {
      return p ? new(p) ::DTTracoId[nElements] : new ::DTTracoId[nElements];
   }
   // Wrapper around operator delete
   static void delete_DTTracoId(void *p) {
      delete ((::DTTracoId*)p);
   }
   static void deleteArray_DTTracoId(void *p) {
      delete [] ((::DTTracoId*)p);
   }
   static void destruct_DTTracoId(void *p) {
      typedef ::DTTracoId current_t;
      ((current_t*)p)->~current_t();
   }
} // end of namespace ROOT for class ::DTTracoId

namespace ROOT {
   // Wrappers around operator new
   static void *new_DTSectCollId(void *p) {
      return  p ? new(p) ::DTSectCollId : new ::DTSectCollId;
   }
   static void *newArray_DTSectCollId(Long_t nElements, void *p) {
      return p ? new(p) ::DTSectCollId[nElements] : new ::DTSectCollId[nElements];
   }
   // Wrapper around operator delete
   static void delete_DTSectCollId(void *p) {
      delete ((::DTSectCollId*)p);
   }
   static void deleteArray_DTSectCollId(void *p) {
      delete [] ((::DTSectCollId*)p);
   }
   static void destruct_DTSectCollId(void *p) {
      typedef ::DTSectCollId current_t;
      ((current_t*)p)->~current_t();
   }
} // end of namespace ROOT for class ::DTSectCollId

namespace ROOT {
   // Wrappers around operator new
   static void *new_GEMDetId(void *p) {
      return  p ? new(p) ::GEMDetId : new ::GEMDetId;
   }
   static void *newArray_GEMDetId(Long_t nElements, void *p) {
      return p ? new(p) ::GEMDetId[nElements] : new ::GEMDetId[nElements];
   }
   // Wrapper around operator delete
   static void delete_GEMDetId(void *p) {
      delete ((::GEMDetId*)p);
   }
   static void deleteArray_GEMDetId(void *p) {
      delete [] ((::GEMDetId*)p);
   }
   static void destruct_GEMDetId(void *p) {
      typedef ::GEMDetId current_t;
      ((current_t*)p)->~current_t();
   }
} // end of namespace ROOT for class ::GEMDetId

namespace ROOT {
   // Wrappers around operator new
   static void *new_ME0DetId(void *p) {
      return  p ? new(p) ::ME0DetId : new ::ME0DetId;
   }
   static void *newArray_ME0DetId(Long_t nElements, void *p) {
      return p ? new(p) ::ME0DetId[nElements] : new ::ME0DetId[nElements];
   }
   // Wrapper around operator delete
   static void delete_ME0DetId(void *p) {
      delete ((::ME0DetId*)p);
   }
   static void deleteArray_ME0DetId(void *p) {
      delete [] ((::ME0DetId*)p);
   }
   static void destruct_ME0DetId(void *p) {
      typedef ::ME0DetId current_t;
      ((current_t*)p)->~current_t();
   }
} // end of namespace ROOT for class ::ME0DetId

namespace {
  void TriggerDictionaryInitialization_DataFormatsMuonDetId_xr_Impl() {
    static const char* headers[] = {
"0",
0
    };
    static const char* includePaths[] = {
"/afs/cern.ch/user/n/nmancill/HSCP/CMSSW_12_0_2/src",
"/cvmfs/cms.cern.ch/slc7_amd64_gcc900/cms/cmssw/CMSSW_12_0_2/src",
"/cvmfs/cms.cern.ch/slc7_amd64_gcc900/external/pcre/8.43-bcolbf/include",
"/cvmfs/cms.cern.ch/slc7_amd64_gcc900/external/bz2lib/1.0.6-bcolbf2/include",
"/cvmfs/cms.cern.ch/slc7_amd64_gcc900/external/libuuid/2.34-bcolbf2/include",
"/cvmfs/cms.cern.ch/slc7_amd64_gcc900/external/xz/5.2.4-cms/include",
"/cvmfs/cms.cern.ch/slc7_amd64_gcc900/external/zlib/1.2.11-bcolbf/include",
"/cvmfs/cms.cern.ch/slc7_amd64_gcc900/external/fmt/7.0.1-llifpc2/include",
"/cvmfs/cms.cern.ch/slc7_amd64_gcc900/external/md5/1.0.0-bcolbf2/include",
"/cvmfs/cms.cern.ch/slc7_amd64_gcc900/external/tinyxml2/6.2.0-llifpc/include",
"/cvmfs/cms.cern.ch/slc7_amd64_gcc900/lcg/root/6.22.08-44c56ac2ea72d4504197c078eb47fd23/include/",
"/afs/cern.ch/user/n/nmancill/HSCP/CMSSW_12_0_2/",
0
    };
    static const char* fwdDeclCode = R"DICTFWDDCLS(
#line 1 "DataFormatsMuonDetId_xr dictionary forward declarations' payload"
#pragma clang diagnostic ignored "-Wkeyword-compat"
#pragma clang diagnostic ignored "-Wignored-attributes"
#pragma clang diagnostic ignored "-Wreturn-type-c-linkage"
extern int __Cling_AutoLoading_Map;
class __attribute__((annotate("$clingAutoload$DataFormats/MuonDetId/interface/DTChamberId.h")))  DTChamberId;
class __attribute__((annotate("$clingAutoload$DataFormats/MuonDetId/interface/DTSuperLayerId.h")))  DTSuperLayerId;
class __attribute__((annotate("$clingAutoload$DataFormats/MuonDetId/interface/DTLayerId.h")))  DTLayerId;
class __attribute__((annotate("$clingAutoload$DataFormats/MuonDetId/interface/DTWireId.h")))  DTWireId;
class __attribute__((annotate("$clingAutoload$DataFormats/MuonDetId/interface/CSCDetId.h")))  CSCDetId;
class __attribute__((annotate("$clingAutoload$DataFormats/MuonDetId/interface/RPCDetId.h")))  RPCDetId;
class __attribute__((annotate("$clingAutoload$DataFormats/MuonDetId/interface/RPCCompDetId.h")))  RPCCompDetId;
class __attribute__((annotate("$clingAutoload$DataFormats/MuonDetId/interface/DTBtiId.h")))  DTBtiId;
class __attribute__((annotate("$clingAutoload$DataFormats/MuonDetId/interface/DTTracoId.h")))  DTTracoId;
class __attribute__((annotate("$clingAutoload$DataFormats/MuonDetId/interface/DTSectCollId.h")))  DTSectCollId;
class __attribute__((annotate("$clingAutoload$DataFormats/MuonDetId/interface/GEMDetId.h")))  GEMDetId;
class __attribute__((annotate("$clingAutoload$DataFormats/MuonDetId/interface/ME0DetId.h")))  ME0DetId;
)DICTFWDDCLS";
    static const char* payloadCode = R"DICTPAYLOAD(
#line 1 "DataFormatsMuonDetId_xr dictionary payload"

#ifndef CMS_DICT_IMPL
  #define CMS_DICT_IMPL 1
#endif
#ifndef _REENTRANT
  #define _REENTRANT 1
#endif
#ifndef GNUSOURCE
  #define GNUSOURCE 1
#endif
#ifndef __STRICT_ANSI__
  #define __STRICT_ANSI__ 1
#endif
#ifndef GNU_GCC
  #define GNU_GCC 1
#endif
#ifndef _GNU_SOURCE
  #define _GNU_SOURCE 1
#endif
#ifndef TBB_USE_GLIBCXX_VERSION
  #define TBB_USE_GLIBCXX_VERSION 90300
#endif
#ifndef TBB_SUPPRESS_DEPRECATED_MESSAGES
  #define TBB_SUPPRESS_DEPRECATED_MESSAGES 1
#endif
#ifndef TBB_PREVIEW_RESUMABLE_TASKS
  #define TBB_PREVIEW_RESUMABLE_TASKS 1
#endif
#ifndef BOOST_SPIRIT_THREADSAFE
  #define BOOST_SPIRIT_THREADSAFE 1
#endif
#ifndef PHOENIX_THREADSAFE
  #define PHOENIX_THREADSAFE 1
#endif
#ifndef BOOST_MATH_DISABLE_STD_FPCLASSIFY
  #define BOOST_MATH_DISABLE_STD_FPCLASSIFY 1
#endif
#ifndef BOOST_UUID_RANDOM_PROVIDER_FORCE_POSIX
  #define BOOST_UUID_RANDOM_PROVIDER_FORCE_POSIX 1
#endif
#ifndef CMSSW_GIT_HASH
  #define CMSSW_GIT_HASH "CMSSW_12_0_2"
#endif
#ifndef PROJECT_NAME
  #define PROJECT_NAME "CMSSW"
#endif
#ifndef PROJECT_VERSION
  #define PROJECT_VERSION "CMSSW_12_0_2"
#endif
#ifndef CMSSW_REFLEX_DICT
  #define CMSSW_REFLEX_DICT 1
#endif

#define _BACKWARD_BACKWARD_WARNING_H
// Inline headers
#include "DataFormats/MuonDetId/interface/DTChamberId.h"
#include <DataFormats/MuonDetId/interface/MuonSubdetId.h>
#include "DataFormats/MuonDetId/interface/DTSuperLayerId.h"
#include "DataFormats/MuonDetId/interface/DTLayerId.h"
#include "DataFormats/MuonDetId/interface/DTWireId.h"
#include "DataFormats/MuonDetId/interface/CSCDetId.h"
#include "DataFormats/MuonDetId/interface/RPCDetId.h"
#include "DataFormats/MuonDetId/interface/RPCCompDetId.h"
#include "DataFormats/MuonDetId/interface/DTBtiId.h"
#include "DataFormats/MuonDetId/interface/DTTracoId.h"
#include "DataFormats/MuonDetId/interface/DTSectCollId.h"
#include "DataFormats/MuonDetId/interface/GEMDetId.h"
#include "DataFormats/MuonDetId/interface/ME0DetId.h"

#undef  _BACKWARD_BACKWARD_WARNING_H
)DICTPAYLOAD";
    static const char* classesHeaders[] = {
"CSCDetId", payloadCode, "@",
"DTBtiId", payloadCode, "@",
"DTChamberId", payloadCode, "@",
"DTLayerId", payloadCode, "@",
"DTSectCollId", payloadCode, "@",
"DTSuperLayerId", payloadCode, "@",
"DTTracoId", payloadCode, "@",
"DTWireId", payloadCode, "@",
"GEMDetId", payloadCode, "@",
"ME0DetId", payloadCode, "@",
"RPCCompDetId", payloadCode, "@",
"RPCDetId", payloadCode, "@",
nullptr
};
    static bool isInitialized = false;
    if (!isInitialized) {
      TROOT::RegisterModule("DataFormatsMuonDetId_xr",
        headers, includePaths, payloadCode, fwdDeclCode,
        TriggerDictionaryInitialization_DataFormatsMuonDetId_xr_Impl, {}, classesHeaders, /*hasCxxModule*/false);
      isInitialized = true;
    }
  }
  static struct DictInit {
    DictInit() {
      TriggerDictionaryInitialization_DataFormatsMuonDetId_xr_Impl();
    }
  } __TheDictionaryInitializer;
}
void TriggerDictionaryInitialization_DataFormatsMuonDetId_xr() {
  TriggerDictionaryInitialization_DataFormatsMuonDetId_xr_Impl();
}
