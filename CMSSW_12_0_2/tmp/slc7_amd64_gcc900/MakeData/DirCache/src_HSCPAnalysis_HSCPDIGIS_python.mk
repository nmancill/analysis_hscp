ifeq ($(strip $(PyHSCPAnalysisHSCPDIGIS)),)
PyHSCPAnalysisHSCPDIGIS := self/src/HSCPAnalysis/HSCPDIGIS/python
src_HSCPAnalysis_HSCPDIGIS_python_parent := src/HSCPAnalysis/HSCPDIGIS
ALL_PYTHON_DIRS += $(patsubst src/%,%,src/HSCPAnalysis/HSCPDIGIS/python)
PyHSCPAnalysisHSCPDIGIS_files := $(patsubst src/HSCPAnalysis/HSCPDIGIS/python/%,%,$(wildcard $(foreach dir,src/HSCPAnalysis/HSCPDIGIS/python ,$(foreach ext,$(SRC_FILES_SUFFIXES),$(dir)/*.$(ext)))))
PyHSCPAnalysisHSCPDIGIS_LOC_USE := self   
PyHSCPAnalysisHSCPDIGIS_PACKAGE := self/src/HSCPAnalysis/HSCPDIGIS/python
ALL_PRODS += PyHSCPAnalysisHSCPDIGIS
PyHSCPAnalysisHSCPDIGIS_INIT_FUNC        += $$(eval $$(call PythonProduct,PyHSCPAnalysisHSCPDIGIS,src/HSCPAnalysis/HSCPDIGIS/python,src_HSCPAnalysis_HSCPDIGIS_python))
else
$(eval $(call MultipleWarningMsg,PyHSCPAnalysisHSCPDIGIS,src/HSCPAnalysis/HSCPDIGIS/python))
endif
ALL_COMMONRULES += src_HSCPAnalysis_HSCPDIGIS_python
src_HSCPAnalysis_HSCPDIGIS_python_INIT_FUNC += $$(eval $$(call CommonProductRules,src_HSCPAnalysis_HSCPDIGIS_python,src/HSCPAnalysis/HSCPDIGIS/python,PYTHON))
