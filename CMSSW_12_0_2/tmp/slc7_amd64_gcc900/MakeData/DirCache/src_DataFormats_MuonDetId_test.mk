ifeq ($(strip $(testCSCDetId)),)
testCSCDetId := self/src/DataFormats/MuonDetId/test
testCSCDetId_files := $(patsubst src/DataFormats/MuonDetId/test/%,%,$(foreach file,testCSCDetId.cc testMuonDetId.cpp,$(eval xfile:=$(wildcard src/DataFormats/MuonDetId/test/$(file)))$(if $(xfile),$(xfile),$(warning No such file exists: src/DataFormats/MuonDetId/test/$(file). Please fix src/DataFormats/MuonDetId/test/BuildFile.))))
testCSCDetId_TEST_RUNNER_CMD :=  testCSCDetId 
testCSCDetId_BuildFile    := $(WORKINGDIR)/cache/bf/src/DataFormats/MuonDetId/test/BuildFile
testCSCDetId_LOC_USE := self   DataFormats/DetId DataFormats/MuonDetId cppunit
testCSCDetId_PACKAGE := self/src/DataFormats/MuonDetId/test
ALL_PRODS += testCSCDetId
testCSCDetId_INIT_FUNC        += $$(eval $$(call Binary,testCSCDetId,src/DataFormats/MuonDetId/test,src_DataFormats_MuonDetId_test,$(SCRAMSTORENAME_BIN),,$(SCRAMSTORENAME_TEST),test,$(SCRAMSTORENAME_LOGS)))
testCSCDetId_CLASS := TEST
else
$(eval $(call MultipleWarningMsg,testCSCDetId,src/DataFormats/MuonDetId/test))
endif
ifeq ($(strip $(testGEMDetId)),)
testGEMDetId := self/src/DataFormats/MuonDetId/test
testGEMDetId_files := $(patsubst src/DataFormats/MuonDetId/test/%,%,$(foreach file,testGEMDetId.cc,$(eval xfile:=$(wildcard src/DataFormats/MuonDetId/test/$(file)))$(if $(xfile),$(xfile),$(warning No such file exists: src/DataFormats/MuonDetId/test/$(file). Please fix src/DataFormats/MuonDetId/test/BuildFile.))))
testGEMDetId_TEST_RUNNER_CMD :=  testGEMDetId 
testGEMDetId_BuildFile    := $(WORKINGDIR)/cache/bf/src/DataFormats/MuonDetId/test/BuildFile
testGEMDetId_LOC_USE := self   DataFormats/DetId DataFormats/MuonDetId
testGEMDetId_PACKAGE := self/src/DataFormats/MuonDetId/test
ALL_PRODS += testGEMDetId
testGEMDetId_INIT_FUNC        += $$(eval $$(call Binary,testGEMDetId,src/DataFormats/MuonDetId/test,src_DataFormats_MuonDetId_test,$(SCRAMSTORENAME_BIN),,$(SCRAMSTORENAME_TEST),test,$(SCRAMSTORENAME_LOGS)))
testGEMDetId_CLASS := TEST
else
$(eval $(call MultipleWarningMsg,testGEMDetId,src/DataFormats/MuonDetId/test))
endif
ifeq ($(strip $(testMuonDetId)),)
testMuonDetId := self/src/DataFormats/MuonDetId/test
testMuonDetId_files := $(patsubst src/DataFormats/MuonDetId/test/%,%,$(foreach file,testDTDetIds.cc testCSCDetId.cc testRPCDetId.cc testRPCCompDetId.cc testCSCTriggerNumbering.cc testME0DetId.cc testMuonDetId.cpp,$(eval xfile:=$(wildcard src/DataFormats/MuonDetId/test/$(file)))$(if $(xfile),$(xfile),$(warning No such file exists: src/DataFormats/MuonDetId/test/$(file). Please fix src/DataFormats/MuonDetId/test/BuildFile.))))
testMuonDetId_TEST_RUNNER_CMD :=  testMuonDetId 
testMuonDetId_BuildFile    := $(WORKINGDIR)/cache/bf/src/DataFormats/MuonDetId/test/BuildFile
testMuonDetId_LOC_FLAGS_CXXFLAGS   := -O2
testMuonDetId_LOC_USE := self   DataFormats/DetId DataFormats/MuonDetId cppunit
testMuonDetId_PACKAGE := self/src/DataFormats/MuonDetId/test
ALL_PRODS += testMuonDetId
testMuonDetId_INIT_FUNC        += $$(eval $$(call Binary,testMuonDetId,src/DataFormats/MuonDetId/test,src_DataFormats_MuonDetId_test,$(SCRAMSTORENAME_BIN),,$(SCRAMSTORENAME_TEST),test,$(SCRAMSTORENAME_LOGS)))
testMuonDetId_CLASS := TEST
else
$(eval $(call MultipleWarningMsg,testMuonDetId,src/DataFormats/MuonDetId/test))
endif
ALL_COMMONRULES += src_DataFormats_MuonDetId_test
src_DataFormats_MuonDetId_test_parent := DataFormats/MuonDetId
src_DataFormats_MuonDetId_test_INIT_FUNC += $$(eval $$(call CommonProductRules,src_DataFormats_MuonDetId_test,src/DataFormats/MuonDetId/test,TEST))
