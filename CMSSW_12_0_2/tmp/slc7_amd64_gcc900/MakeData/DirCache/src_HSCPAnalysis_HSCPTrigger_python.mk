ifeq ($(strip $(PyHSCPAnalysisHSCPTrigger)),)
PyHSCPAnalysisHSCPTrigger := self/src/HSCPAnalysis/HSCPTrigger/python
src_HSCPAnalysis_HSCPTrigger_python_parent := src/HSCPAnalysis/HSCPTrigger
ALL_PYTHON_DIRS += $(patsubst src/%,%,src/HSCPAnalysis/HSCPTrigger/python)
PyHSCPAnalysisHSCPTrigger_files := $(patsubst src/HSCPAnalysis/HSCPTrigger/python/%,%,$(wildcard $(foreach dir,src/HSCPAnalysis/HSCPTrigger/python ,$(foreach ext,$(SRC_FILES_SUFFIXES),$(dir)/*.$(ext)))))
PyHSCPAnalysisHSCPTrigger_LOC_USE := self   
PyHSCPAnalysisHSCPTrigger_PACKAGE := self/src/HSCPAnalysis/HSCPTrigger/python
ALL_PRODS += PyHSCPAnalysisHSCPTrigger
PyHSCPAnalysisHSCPTrigger_INIT_FUNC        += $$(eval $$(call PythonProduct,PyHSCPAnalysisHSCPTrigger,src/HSCPAnalysis/HSCPTrigger/python,src_HSCPAnalysis_HSCPTrigger_python))
else
$(eval $(call MultipleWarningMsg,PyHSCPAnalysisHSCPTrigger,src/HSCPAnalysis/HSCPTrigger/python))
endif
ALL_COMMONRULES += src_HSCPAnalysis_HSCPTrigger_python
src_HSCPAnalysis_HSCPTrigger_python_INIT_FUNC += $$(eval $$(call CommonProductRules,src_HSCPAnalysis_HSCPTrigger_python,src/HSCPAnalysis/HSCPTrigger/python,PYTHON))
