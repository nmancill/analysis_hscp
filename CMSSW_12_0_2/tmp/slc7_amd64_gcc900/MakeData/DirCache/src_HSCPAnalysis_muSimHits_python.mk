ifeq ($(strip $(PyHSCPAnalysismuSimHits)),)
PyHSCPAnalysismuSimHits := self/src/HSCPAnalysis/muSimHits/python
src_HSCPAnalysis_muSimHits_python_parent := src/HSCPAnalysis/muSimHits
ALL_PYTHON_DIRS += $(patsubst src/%,%,src/HSCPAnalysis/muSimHits/python)
PyHSCPAnalysismuSimHits_files := $(patsubst src/HSCPAnalysis/muSimHits/python/%,%,$(wildcard $(foreach dir,src/HSCPAnalysis/muSimHits/python ,$(foreach ext,$(SRC_FILES_SUFFIXES),$(dir)/*.$(ext)))))
PyHSCPAnalysismuSimHits_LOC_USE := self   
PyHSCPAnalysismuSimHits_PACKAGE := self/src/HSCPAnalysis/muSimHits/python
ALL_PRODS += PyHSCPAnalysismuSimHits
PyHSCPAnalysismuSimHits_INIT_FUNC        += $$(eval $$(call PythonProduct,PyHSCPAnalysismuSimHits,src/HSCPAnalysis/muSimHits/python,src_HSCPAnalysis_muSimHits_python))
else
$(eval $(call MultipleWarningMsg,PyHSCPAnalysismuSimHits,src/HSCPAnalysis/muSimHits/python))
endif
ALL_COMMONRULES += src_HSCPAnalysis_muSimHits_python
src_HSCPAnalysis_muSimHits_python_INIT_FUNC += $$(eval $$(call CommonProductRules,src_HSCPAnalysis_muSimHits_python,src/HSCPAnalysis/muSimHits/python,PYTHON))
