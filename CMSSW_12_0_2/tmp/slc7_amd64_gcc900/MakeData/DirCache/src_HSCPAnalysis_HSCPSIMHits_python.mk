ifeq ($(strip $(PyHSCPAnalysisHSCPSIMHits)),)
PyHSCPAnalysisHSCPSIMHits := self/src/HSCPAnalysis/HSCPSIMHits/python
src_HSCPAnalysis_HSCPSIMHits_python_parent := src/HSCPAnalysis/HSCPSIMHits
ALL_PYTHON_DIRS += $(patsubst src/%,%,src/HSCPAnalysis/HSCPSIMHits/python)
PyHSCPAnalysisHSCPSIMHits_files := $(patsubst src/HSCPAnalysis/HSCPSIMHits/python/%,%,$(wildcard $(foreach dir,src/HSCPAnalysis/HSCPSIMHits/python ,$(foreach ext,$(SRC_FILES_SUFFIXES),$(dir)/*.$(ext)))))
PyHSCPAnalysisHSCPSIMHits_LOC_USE := self   
PyHSCPAnalysisHSCPSIMHits_PACKAGE := self/src/HSCPAnalysis/HSCPSIMHits/python
ALL_PRODS += PyHSCPAnalysisHSCPSIMHits
PyHSCPAnalysisHSCPSIMHits_INIT_FUNC        += $$(eval $$(call PythonProduct,PyHSCPAnalysisHSCPSIMHits,src/HSCPAnalysis/HSCPSIMHits/python,src_HSCPAnalysis_HSCPSIMHits_python))
else
$(eval $(call MultipleWarningMsg,PyHSCPAnalysisHSCPSIMHits,src/HSCPAnalysis/HSCPSIMHits/python))
endif
ALL_COMMONRULES += src_HSCPAnalysis_HSCPSIMHits_python
src_HSCPAnalysis_HSCPSIMHits_python_INIT_FUNC += $$(eval $$(call CommonProductRules,src_HSCPAnalysis_HSCPSIMHits_python,src/HSCPAnalysis/HSCPSIMHits/python,PYTHON))
