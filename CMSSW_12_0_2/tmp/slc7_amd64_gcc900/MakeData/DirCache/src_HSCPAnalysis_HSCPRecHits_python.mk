ifeq ($(strip $(PyHSCPAnalysisHSCPRecHits)),)
PyHSCPAnalysisHSCPRecHits := self/src/HSCPAnalysis/HSCPRecHits/python
src_HSCPAnalysis_HSCPRecHits_python_parent := src/HSCPAnalysis/HSCPRecHits
ALL_PYTHON_DIRS += $(patsubst src/%,%,src/HSCPAnalysis/HSCPRecHits/python)
PyHSCPAnalysisHSCPRecHits_files := $(patsubst src/HSCPAnalysis/HSCPRecHits/python/%,%,$(wildcard $(foreach dir,src/HSCPAnalysis/HSCPRecHits/python ,$(foreach ext,$(SRC_FILES_SUFFIXES),$(dir)/*.$(ext)))))
PyHSCPAnalysisHSCPRecHits_LOC_USE := self   
PyHSCPAnalysisHSCPRecHits_PACKAGE := self/src/HSCPAnalysis/HSCPRecHits/python
ALL_PRODS += PyHSCPAnalysisHSCPRecHits
PyHSCPAnalysisHSCPRecHits_INIT_FUNC        += $$(eval $$(call PythonProduct,PyHSCPAnalysisHSCPRecHits,src/HSCPAnalysis/HSCPRecHits/python,src_HSCPAnalysis_HSCPRecHits_python))
else
$(eval $(call MultipleWarningMsg,PyHSCPAnalysisHSCPRecHits,src/HSCPAnalysis/HSCPRecHits/python))
endif
ALL_COMMONRULES += src_HSCPAnalysis_HSCPRecHits_python
src_HSCPAnalysis_HSCPRecHits_python_INIT_FUNC += $$(eval $$(call CommonProductRules,src_HSCPAnalysis_HSCPRecHits_python,src/HSCPAnalysis/HSCPRecHits/python,PYTHON))
