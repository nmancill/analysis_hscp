ALL_SUBSYSTEMS+=DataFormats
subdirs_src_DataFormats = src_DataFormats_MuonDetId
subdirs_src += src_DataFormats
ALL_PACKAGES += DataFormats/MuonDetId
subdirs_src_DataFormats_MuonDetId := src_DataFormats_MuonDetId_src src_DataFormats_MuonDetId_test
ALL_SUBSYSTEMS+=HSCPAnalysis
subdirs_src_HSCPAnalysis = src_HSCPAnalysis_HSCPDIGIS src_HSCPAnalysis_HSCPRecHits src_HSCPAnalysis_HSCPSIMHits src_HSCPAnalysis_HSCPTrigger src_HSCPAnalysis_muSimHits
subdirs_src += src_HSCPAnalysis
ALL_PACKAGES += HSCPAnalysis/HSCPDIGIS
subdirs_src_HSCPAnalysis_HSCPDIGIS := src_HSCPAnalysis_HSCPDIGIS_plugins src_HSCPAnalysis_HSCPDIGIS_python src_HSCPAnalysis_HSCPDIGIS_test
ifeq ($(strip $(PyHSCPAnalysisHSCPDIGIS)),)
PyHSCPAnalysisHSCPDIGIS := self/src/HSCPAnalysis/HSCPDIGIS/python
src_HSCPAnalysis_HSCPDIGIS_python_parent := src/HSCPAnalysis/HSCPDIGIS
ALL_PYTHON_DIRS += $(patsubst src/%,%,src/HSCPAnalysis/HSCPDIGIS/python)
PyHSCPAnalysisHSCPDIGIS_files := $(patsubst src/HSCPAnalysis/HSCPDIGIS/python/%,%,$(wildcard $(foreach dir,src/HSCPAnalysis/HSCPDIGIS/python ,$(foreach ext,$(SRC_FILES_SUFFIXES),$(dir)/*.$(ext)))))
PyHSCPAnalysisHSCPDIGIS_LOC_USE := self   
PyHSCPAnalysisHSCPDIGIS_PACKAGE := self/src/HSCPAnalysis/HSCPDIGIS/python
ALL_PRODS += PyHSCPAnalysisHSCPDIGIS
PyHSCPAnalysisHSCPDIGIS_INIT_FUNC        += $$(eval $$(call PythonProduct,PyHSCPAnalysisHSCPDIGIS,src/HSCPAnalysis/HSCPDIGIS/python,src_HSCPAnalysis_HSCPDIGIS_python))
else
$(eval $(call MultipleWarningMsg,PyHSCPAnalysisHSCPDIGIS,src/HSCPAnalysis/HSCPDIGIS/python))
endif
ALL_COMMONRULES += src_HSCPAnalysis_HSCPDIGIS_python
src_HSCPAnalysis_HSCPDIGIS_python_INIT_FUNC += $$(eval $$(call CommonProductRules,src_HSCPAnalysis_HSCPDIGIS_python,src/HSCPAnalysis/HSCPDIGIS/python,PYTHON))
ALL_PACKAGES += HSCPAnalysis/HSCPRecHits
subdirs_src_HSCPAnalysis_HSCPRecHits := src_HSCPAnalysis_HSCPRecHits_plugins src_HSCPAnalysis_HSCPRecHits_python src_HSCPAnalysis_HSCPRecHits_test
ifeq ($(strip $(PyHSCPAnalysisHSCPRecHits)),)
PyHSCPAnalysisHSCPRecHits := self/src/HSCPAnalysis/HSCPRecHits/python
src_HSCPAnalysis_HSCPRecHits_python_parent := src/HSCPAnalysis/HSCPRecHits
ALL_PYTHON_DIRS += $(patsubst src/%,%,src/HSCPAnalysis/HSCPRecHits/python)
PyHSCPAnalysisHSCPRecHits_files := $(patsubst src/HSCPAnalysis/HSCPRecHits/python/%,%,$(wildcard $(foreach dir,src/HSCPAnalysis/HSCPRecHits/python ,$(foreach ext,$(SRC_FILES_SUFFIXES),$(dir)/*.$(ext)))))
PyHSCPAnalysisHSCPRecHits_LOC_USE := self   
PyHSCPAnalysisHSCPRecHits_PACKAGE := self/src/HSCPAnalysis/HSCPRecHits/python
ALL_PRODS += PyHSCPAnalysisHSCPRecHits
PyHSCPAnalysisHSCPRecHits_INIT_FUNC        += $$(eval $$(call PythonProduct,PyHSCPAnalysisHSCPRecHits,src/HSCPAnalysis/HSCPRecHits/python,src_HSCPAnalysis_HSCPRecHits_python))
else
$(eval $(call MultipleWarningMsg,PyHSCPAnalysisHSCPRecHits,src/HSCPAnalysis/HSCPRecHits/python))
endif
ALL_COMMONRULES += src_HSCPAnalysis_HSCPRecHits_python
src_HSCPAnalysis_HSCPRecHits_python_INIT_FUNC += $$(eval $$(call CommonProductRules,src_HSCPAnalysis_HSCPRecHits_python,src/HSCPAnalysis/HSCPRecHits/python,PYTHON))
ALL_PACKAGES += HSCPAnalysis/HSCPSIMHits
subdirs_src_HSCPAnalysis_HSCPSIMHits := src_HSCPAnalysis_HSCPSIMHits_plugins src_HSCPAnalysis_HSCPSIMHits_python src_HSCPAnalysis_HSCPSIMHits_test
ifeq ($(strip $(PyHSCPAnalysisHSCPSIMHits)),)
PyHSCPAnalysisHSCPSIMHits := self/src/HSCPAnalysis/HSCPSIMHits/python
src_HSCPAnalysis_HSCPSIMHits_python_parent := src/HSCPAnalysis/HSCPSIMHits
ALL_PYTHON_DIRS += $(patsubst src/%,%,src/HSCPAnalysis/HSCPSIMHits/python)
PyHSCPAnalysisHSCPSIMHits_files := $(patsubst src/HSCPAnalysis/HSCPSIMHits/python/%,%,$(wildcard $(foreach dir,src/HSCPAnalysis/HSCPSIMHits/python ,$(foreach ext,$(SRC_FILES_SUFFIXES),$(dir)/*.$(ext)))))
PyHSCPAnalysisHSCPSIMHits_LOC_USE := self   
PyHSCPAnalysisHSCPSIMHits_PACKAGE := self/src/HSCPAnalysis/HSCPSIMHits/python
ALL_PRODS += PyHSCPAnalysisHSCPSIMHits
PyHSCPAnalysisHSCPSIMHits_INIT_FUNC        += $$(eval $$(call PythonProduct,PyHSCPAnalysisHSCPSIMHits,src/HSCPAnalysis/HSCPSIMHits/python,src_HSCPAnalysis_HSCPSIMHits_python))
else
$(eval $(call MultipleWarningMsg,PyHSCPAnalysisHSCPSIMHits,src/HSCPAnalysis/HSCPSIMHits/python))
endif
ALL_COMMONRULES += src_HSCPAnalysis_HSCPSIMHits_python
src_HSCPAnalysis_HSCPSIMHits_python_INIT_FUNC += $$(eval $$(call CommonProductRules,src_HSCPAnalysis_HSCPSIMHits_python,src/HSCPAnalysis/HSCPSIMHits/python,PYTHON))
ALL_PACKAGES += HSCPAnalysis/HSCPTrigger
subdirs_src_HSCPAnalysis_HSCPTrigger := src_HSCPAnalysis_HSCPTrigger_plugins src_HSCPAnalysis_HSCPTrigger_python src_HSCPAnalysis_HSCPTrigger_test
ifeq ($(strip $(PyHSCPAnalysisHSCPTrigger)),)
PyHSCPAnalysisHSCPTrigger := self/src/HSCPAnalysis/HSCPTrigger/python
src_HSCPAnalysis_HSCPTrigger_python_parent := src/HSCPAnalysis/HSCPTrigger
ALL_PYTHON_DIRS += $(patsubst src/%,%,src/HSCPAnalysis/HSCPTrigger/python)
PyHSCPAnalysisHSCPTrigger_files := $(patsubst src/HSCPAnalysis/HSCPTrigger/python/%,%,$(wildcard $(foreach dir,src/HSCPAnalysis/HSCPTrigger/python ,$(foreach ext,$(SRC_FILES_SUFFIXES),$(dir)/*.$(ext)))))
PyHSCPAnalysisHSCPTrigger_LOC_USE := self   
PyHSCPAnalysisHSCPTrigger_PACKAGE := self/src/HSCPAnalysis/HSCPTrigger/python
ALL_PRODS += PyHSCPAnalysisHSCPTrigger
PyHSCPAnalysisHSCPTrigger_INIT_FUNC        += $$(eval $$(call PythonProduct,PyHSCPAnalysisHSCPTrigger,src/HSCPAnalysis/HSCPTrigger/python,src_HSCPAnalysis_HSCPTrigger_python))
else
$(eval $(call MultipleWarningMsg,PyHSCPAnalysisHSCPTrigger,src/HSCPAnalysis/HSCPTrigger/python))
endif
ALL_COMMONRULES += src_HSCPAnalysis_HSCPTrigger_python
src_HSCPAnalysis_HSCPTrigger_python_INIT_FUNC += $$(eval $$(call CommonProductRules,src_HSCPAnalysis_HSCPTrigger_python,src/HSCPAnalysis/HSCPTrigger/python,PYTHON))
ALL_PACKAGES += HSCPAnalysis/muSimHits
subdirs_src_HSCPAnalysis_muSimHits := src_HSCPAnalysis_muSimHits_plugins src_HSCPAnalysis_muSimHits_python src_HSCPAnalysis_muSimHits_test
ifeq ($(strip $(PyHSCPAnalysismuSimHits)),)
PyHSCPAnalysismuSimHits := self/src/HSCPAnalysis/muSimHits/python
src_HSCPAnalysis_muSimHits_python_parent := src/HSCPAnalysis/muSimHits
ALL_PYTHON_DIRS += $(patsubst src/%,%,src/HSCPAnalysis/muSimHits/python)
PyHSCPAnalysismuSimHits_files := $(patsubst src/HSCPAnalysis/muSimHits/python/%,%,$(wildcard $(foreach dir,src/HSCPAnalysis/muSimHits/python ,$(foreach ext,$(SRC_FILES_SUFFIXES),$(dir)/*.$(ext)))))
PyHSCPAnalysismuSimHits_LOC_USE := self   
PyHSCPAnalysismuSimHits_PACKAGE := self/src/HSCPAnalysis/muSimHits/python
ALL_PRODS += PyHSCPAnalysismuSimHits
PyHSCPAnalysismuSimHits_INIT_FUNC        += $$(eval $$(call PythonProduct,PyHSCPAnalysismuSimHits,src/HSCPAnalysis/muSimHits/python,src_HSCPAnalysis_muSimHits_python))
else
$(eval $(call MultipleWarningMsg,PyHSCPAnalysismuSimHits,src/HSCPAnalysis/muSimHits/python))
endif
ALL_COMMONRULES += src_HSCPAnalysis_muSimHits_python
src_HSCPAnalysis_muSimHits_python_INIT_FUNC += $$(eval $$(call CommonProductRules,src_HSCPAnalysis_muSimHits_python,src/HSCPAnalysis/muSimHits/python,PYTHON))
ifeq ($(strip $(testCSCDetId)),)
testCSCDetId := self/src/DataFormats/MuonDetId/test
testCSCDetId_files := $(patsubst src/DataFormats/MuonDetId/test/%,%,$(foreach file,testCSCDetId.cc testMuonDetId.cpp,$(eval xfile:=$(wildcard src/DataFormats/MuonDetId/test/$(file)))$(if $(xfile),$(xfile),$(warning No such file exists: src/DataFormats/MuonDetId/test/$(file). Please fix src/DataFormats/MuonDetId/test/BuildFile.))))
testCSCDetId_TEST_RUNNER_CMD :=  testCSCDetId 
testCSCDetId_BuildFile    := $(WORKINGDIR)/cache/bf/src/DataFormats/MuonDetId/test/BuildFile
testCSCDetId_LOC_USE := self   DataFormats/DetId DataFormats/MuonDetId cppunit
testCSCDetId_PACKAGE := self/src/DataFormats/MuonDetId/test
ALL_PRODS += testCSCDetId
testCSCDetId_INIT_FUNC        += $$(eval $$(call Binary,testCSCDetId,src/DataFormats/MuonDetId/test,src_DataFormats_MuonDetId_test,$(SCRAMSTORENAME_BIN),,$(SCRAMSTORENAME_TEST),test,$(SCRAMSTORENAME_LOGS)))
testCSCDetId_CLASS := TEST
else
$(eval $(call MultipleWarningMsg,testCSCDetId,src/DataFormats/MuonDetId/test))
endif
ifeq ($(strip $(testGEMDetId)),)
testGEMDetId := self/src/DataFormats/MuonDetId/test
testGEMDetId_files := $(patsubst src/DataFormats/MuonDetId/test/%,%,$(foreach file,testGEMDetId.cc,$(eval xfile:=$(wildcard src/DataFormats/MuonDetId/test/$(file)))$(if $(xfile),$(xfile),$(warning No such file exists: src/DataFormats/MuonDetId/test/$(file). Please fix src/DataFormats/MuonDetId/test/BuildFile.))))
testGEMDetId_TEST_RUNNER_CMD :=  testGEMDetId 
testGEMDetId_BuildFile    := $(WORKINGDIR)/cache/bf/src/DataFormats/MuonDetId/test/BuildFile
testGEMDetId_LOC_USE := self   DataFormats/DetId DataFormats/MuonDetId
testGEMDetId_PACKAGE := self/src/DataFormats/MuonDetId/test
ALL_PRODS += testGEMDetId
testGEMDetId_INIT_FUNC        += $$(eval $$(call Binary,testGEMDetId,src/DataFormats/MuonDetId/test,src_DataFormats_MuonDetId_test,$(SCRAMSTORENAME_BIN),,$(SCRAMSTORENAME_TEST),test,$(SCRAMSTORENAME_LOGS)))
testGEMDetId_CLASS := TEST
else
$(eval $(call MultipleWarningMsg,testGEMDetId,src/DataFormats/MuonDetId/test))
endif
ifeq ($(strip $(testMuonDetId)),)
testMuonDetId := self/src/DataFormats/MuonDetId/test
testMuonDetId_files := $(patsubst src/DataFormats/MuonDetId/test/%,%,$(foreach file,testDTDetIds.cc testCSCDetId.cc testRPCDetId.cc testRPCCompDetId.cc testCSCTriggerNumbering.cc testME0DetId.cc testMuonDetId.cpp,$(eval xfile:=$(wildcard src/DataFormats/MuonDetId/test/$(file)))$(if $(xfile),$(xfile),$(warning No such file exists: src/DataFormats/MuonDetId/test/$(file). Please fix src/DataFormats/MuonDetId/test/BuildFile.))))
testMuonDetId_TEST_RUNNER_CMD :=  testMuonDetId 
testMuonDetId_BuildFile    := $(WORKINGDIR)/cache/bf/src/DataFormats/MuonDetId/test/BuildFile
testMuonDetId_LOC_FLAGS_CXXFLAGS   := -O2
testMuonDetId_LOC_USE := self   DataFormats/DetId DataFormats/MuonDetId cppunit
testMuonDetId_PACKAGE := self/src/DataFormats/MuonDetId/test
ALL_PRODS += testMuonDetId
testMuonDetId_INIT_FUNC        += $$(eval $$(call Binary,testMuonDetId,src/DataFormats/MuonDetId/test,src_DataFormats_MuonDetId_test,$(SCRAMSTORENAME_BIN),,$(SCRAMSTORENAME_TEST),test,$(SCRAMSTORENAME_LOGS)))
testMuonDetId_CLASS := TEST
else
$(eval $(call MultipleWarningMsg,testMuonDetId,src/DataFormats/MuonDetId/test))
endif
ALL_COMMONRULES += src_DataFormats_MuonDetId_test
src_DataFormats_MuonDetId_test_parent := DataFormats/MuonDetId
src_DataFormats_MuonDetId_test_INIT_FUNC += $$(eval $$(call CommonProductRules,src_DataFormats_MuonDetId_test,src/DataFormats/MuonDetId/test,TEST))
