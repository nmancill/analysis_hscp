ifeq ($(strip $(DataFormats/MuonDetId)),)
ALL_COMMONRULES += src_DataFormats_MuonDetId_src
src_DataFormats_MuonDetId_src_parent := DataFormats/MuonDetId
src_DataFormats_MuonDetId_src_INIT_FUNC := $$(eval $$(call CommonProductRules,src_DataFormats_MuonDetId_src,src/DataFormats/MuonDetId/src,LIBRARY))
DataFormatsMuonDetId := self/DataFormats/MuonDetId
DataFormats/MuonDetId := DataFormatsMuonDetId
DataFormatsMuonDetId_files := $(patsubst src/DataFormats/MuonDetId/src/%,%,$(wildcard $(foreach dir,src/DataFormats/MuonDetId/src ,$(foreach ext,$(SRC_FILES_SUFFIXES),$(dir)/*.$(ext)))))
DataFormatsMuonDetId_BuildFile    := $(WORKINGDIR)/cache/bf/src/DataFormats/MuonDetId/BuildFile
DataFormatsMuonDetId_LOC_USE := self   DataFormats/DetId FWCore/Utilities
DataFormatsMuonDetId_LCGDICTS  := x 
DataFormatsMuonDetId_PRE_INIT_FUNC += $$(eval $$(call LCGDict,DataFormatsMuonDetId,src/DataFormats/MuonDetId/src/classes.h,src/DataFormats/MuonDetId/src/classes_def.xml,$(SCRAMSTORENAME_LIB),$(GENREFLEX_ARGS) $(root_EX_FLAGS_GENREFLEX_FAILES_ON_WARNS)))
DataFormatsMuonDetId_EX_LIB   := DataFormatsMuonDetId
DataFormatsMuonDetId_EX_USE   := $(foreach d,$(DataFormatsMuonDetId_LOC_USE),$(if $($(d)_EX_FLAGS_NO_RECURSIVE_EXPORT),,$d))
DataFormatsMuonDetId_PACKAGE := self/src/DataFormats/MuonDetId/src
ALL_PRODS += DataFormatsMuonDetId
DataFormatsMuonDetId_CLASS := LIBRARY
DataFormats/MuonDetId_forbigobj+=DataFormatsMuonDetId
DataFormatsMuonDetId_INIT_FUNC        += $$(eval $$(call Library,DataFormatsMuonDetId,src/DataFormats/MuonDetId/src,src_DataFormats_MuonDetId_src,$(SCRAMSTORENAME_BIN),,$(SCRAMSTORENAME_LIB),$(SCRAMSTORENAME_LOGS),))
endif
ifeq ($(strip $(HSCPAnalysisHSCPDIGISAuto)),)
HSCPAnalysisHSCPDIGISAuto := self/src/HSCPAnalysis/HSCPDIGIS/plugins
PLUGINS:=yes
HSCPAnalysisHSCPDIGISAuto_files := $(patsubst src/HSCPAnalysis/HSCPDIGIS/plugins/%,%,$(wildcard $(foreach dir,src/HSCPAnalysis/HSCPDIGIS/plugins ,$(foreach ext,$(SRC_FILES_SUFFIXES),$(dir)/*.$(ext)))))
HSCPAnalysisHSCPDIGISAuto_BuildFile    := $(WORKINGDIR)/cache/bf/src/HSCPAnalysis/HSCPDIGIS/plugins/BuildFile
HSCPAnalysisHSCPDIGISAuto_LOC_USE := self   FWCore/Framework FWCore/PluginManager FWCore/ParameterSet SimDataFormats/RPCDigiSimLink DataFormats/DetId DataFormats/MuonDetId Geometry/CommonDetUnit RecoMuon/DetLayers rootcore rootgraphics root CommonTools/UtilAlgos DataFormats/RPCDigi Geometry/RPCGeometry
HSCPAnalysisHSCPDIGISAuto_PRE_INIT_FUNC += $$(eval $$(call edmPlugin,HSCPAnalysisHSCPDIGISAuto,HSCPAnalysisHSCPDIGISAuto,$(SCRAMSTORENAME_LIB),src/HSCPAnalysis/HSCPDIGIS/plugins))
HSCPAnalysisHSCPDIGISAuto_PACKAGE := self/src/HSCPAnalysis/HSCPDIGIS/plugins
ALL_PRODS += HSCPAnalysisHSCPDIGISAuto
HSCPAnalysis/HSCPDIGIS_forbigobj+=HSCPAnalysisHSCPDIGISAuto
HSCPAnalysisHSCPDIGISAuto_INIT_FUNC        += $$(eval $$(call Library,HSCPAnalysisHSCPDIGISAuto,src/HSCPAnalysis/HSCPDIGIS/plugins,src_HSCPAnalysis_HSCPDIGIS_plugins,$(SCRAMSTORENAME_BIN),,$(SCRAMSTORENAME_LIB),$(SCRAMSTORENAME_LOGS),edm))
HSCPAnalysisHSCPDIGISAuto_CLASS := LIBRARY
else
$(eval $(call MultipleWarningMsg,HSCPAnalysisHSCPDIGISAuto,src/HSCPAnalysis/HSCPDIGIS/plugins))
endif
ALL_COMMONRULES += src_HSCPAnalysis_HSCPDIGIS_plugins
src_HSCPAnalysis_HSCPDIGIS_plugins_parent := HSCPAnalysis/HSCPDIGIS
src_HSCPAnalysis_HSCPDIGIS_plugins_INIT_FUNC += $$(eval $$(call CommonProductRules,src_HSCPAnalysis_HSCPDIGIS_plugins,src/HSCPAnalysis/HSCPDIGIS/plugins,PLUGINS))
ifeq ($(strip $(HSCPAnalysisHSCPRecHitsAuto)),)
HSCPAnalysisHSCPRecHitsAuto := self/src/HSCPAnalysis/HSCPRecHits/plugins
PLUGINS:=yes
HSCPAnalysisHSCPRecHitsAuto_files := $(patsubst src/HSCPAnalysis/HSCPRecHits/plugins/%,%,$(wildcard $(foreach dir,src/HSCPAnalysis/HSCPRecHits/plugins ,$(foreach ext,$(SRC_FILES_SUFFIXES),$(dir)/*.$(ext)))))
HSCPAnalysisHSCPRecHitsAuto_BuildFile    := $(WORKINGDIR)/cache/bf/src/HSCPAnalysis/HSCPRecHits/plugins/BuildFile
HSCPAnalysisHSCPRecHitsAuto_LOC_USE := self   FWCore/Framework FWCore/PluginManager FWCore/ParameterSet SimDataFormats/RPCDigiSimLink SimDataFormats/Vertex SimDataFormats/GeneratorProducts SimGeneral/HepPDTRecord DataFormats/DetId DataFormats/MuonDetId Geometry/CommonDetUnit RecoMuon/DetLayers rootcore rootgraphics root CommonTools/UtilAlgos DataFormats/RPCDigi Geometry/RPCGeometry Geometry/Records
HSCPAnalysisHSCPRecHitsAuto_PRE_INIT_FUNC += $$(eval $$(call edmPlugin,HSCPAnalysisHSCPRecHitsAuto,HSCPAnalysisHSCPRecHitsAuto,$(SCRAMSTORENAME_LIB),src/HSCPAnalysis/HSCPRecHits/plugins))
HSCPAnalysisHSCPRecHitsAuto_PACKAGE := self/src/HSCPAnalysis/HSCPRecHits/plugins
ALL_PRODS += HSCPAnalysisHSCPRecHitsAuto
HSCPAnalysis/HSCPRecHits_forbigobj+=HSCPAnalysisHSCPRecHitsAuto
HSCPAnalysisHSCPRecHitsAuto_INIT_FUNC        += $$(eval $$(call Library,HSCPAnalysisHSCPRecHitsAuto,src/HSCPAnalysis/HSCPRecHits/plugins,src_HSCPAnalysis_HSCPRecHits_plugins,$(SCRAMSTORENAME_BIN),,$(SCRAMSTORENAME_LIB),$(SCRAMSTORENAME_LOGS),edm))
HSCPAnalysisHSCPRecHitsAuto_CLASS := LIBRARY
else
$(eval $(call MultipleWarningMsg,HSCPAnalysisHSCPRecHitsAuto,src/HSCPAnalysis/HSCPRecHits/plugins))
endif
ALL_COMMONRULES += src_HSCPAnalysis_HSCPRecHits_plugins
src_HSCPAnalysis_HSCPRecHits_plugins_parent := HSCPAnalysis/HSCPRecHits
src_HSCPAnalysis_HSCPRecHits_plugins_INIT_FUNC += $$(eval $$(call CommonProductRules,src_HSCPAnalysis_HSCPRecHits_plugins,src/HSCPAnalysis/HSCPRecHits/plugins,PLUGINS))
ifeq ($(strip $(HSCPAnalysisHSCPSIMHitsAuto)),)
HSCPAnalysisHSCPSIMHitsAuto := self/src/HSCPAnalysis/HSCPSIMHits/plugins
PLUGINS:=yes
HSCPAnalysisHSCPSIMHitsAuto_files := $(patsubst src/HSCPAnalysis/HSCPSIMHits/plugins/%,%,$(wildcard $(foreach dir,src/HSCPAnalysis/HSCPSIMHits/plugins ,$(foreach ext,$(SRC_FILES_SUFFIXES),$(dir)/*.$(ext)))))
HSCPAnalysisHSCPSIMHitsAuto_BuildFile    := $(WORKINGDIR)/cache/bf/src/HSCPAnalysis/HSCPSIMHits/plugins/BuildFile
HSCPAnalysisHSCPSIMHitsAuto_LOC_USE := self   FWCore/Framework FWCore/PluginManager FWCore/ParameterSet SimDataFormats/GeneratorProducts DataFormats/DetId DataFormats/GeometrySurface DataFormats/GeometryVector DataFormats/MuonDetId Geometry/CommonDetUnit Geometry/RPCGeometry Geometry/Records RecoMuon/TrackingTools CommonTools/UtilAlgos
HSCPAnalysisHSCPSIMHitsAuto_PRE_INIT_FUNC += $$(eval $$(call edmPlugin,HSCPAnalysisHSCPSIMHitsAuto,HSCPAnalysisHSCPSIMHitsAuto,$(SCRAMSTORENAME_LIB),src/HSCPAnalysis/HSCPSIMHits/plugins))
HSCPAnalysisHSCPSIMHitsAuto_PACKAGE := self/src/HSCPAnalysis/HSCPSIMHits/plugins
ALL_PRODS += HSCPAnalysisHSCPSIMHitsAuto
HSCPAnalysis/HSCPSIMHits_forbigobj+=HSCPAnalysisHSCPSIMHitsAuto
HSCPAnalysisHSCPSIMHitsAuto_INIT_FUNC        += $$(eval $$(call Library,HSCPAnalysisHSCPSIMHitsAuto,src/HSCPAnalysis/HSCPSIMHits/plugins,src_HSCPAnalysis_HSCPSIMHits_plugins,$(SCRAMSTORENAME_BIN),,$(SCRAMSTORENAME_LIB),$(SCRAMSTORENAME_LOGS),edm))
HSCPAnalysisHSCPSIMHitsAuto_CLASS := LIBRARY
else
$(eval $(call MultipleWarningMsg,HSCPAnalysisHSCPSIMHitsAuto,src/HSCPAnalysis/HSCPSIMHits/plugins))
endif
ALL_COMMONRULES += src_HSCPAnalysis_HSCPSIMHits_plugins
src_HSCPAnalysis_HSCPSIMHits_plugins_parent := HSCPAnalysis/HSCPSIMHits
src_HSCPAnalysis_HSCPSIMHits_plugins_INIT_FUNC += $$(eval $$(call CommonProductRules,src_HSCPAnalysis_HSCPSIMHits_plugins,src/HSCPAnalysis/HSCPSIMHits/plugins,PLUGINS))
ifeq ($(strip $(HSCPAnalysisHSCPTriggerAuto)),)
HSCPAnalysisHSCPTriggerAuto := self/src/HSCPAnalysis/HSCPTrigger/plugins
PLUGINS:=yes
HSCPAnalysisHSCPTriggerAuto_files := $(patsubst src/HSCPAnalysis/HSCPTrigger/plugins/%,%,$(wildcard $(foreach dir,src/HSCPAnalysis/HSCPTrigger/plugins ,$(foreach ext,$(SRC_FILES_SUFFIXES),$(dir)/*.$(ext)))))
HSCPAnalysisHSCPTriggerAuto_BuildFile    := $(WORKINGDIR)/cache/bf/src/HSCPAnalysis/HSCPTrigger/plugins/BuildFile
HSCPAnalysisHSCPTriggerAuto_LOC_USE := self   FWCore/Framework FWCore/PluginManager FWCore/ParameterSet SimDataFormats/RPCDigiSimLink SimDataFormats/Vertex SimDataFormats/GeneratorProducts SimGeneral/HepPDTRecord DataFormats/DetId DataFormats/MuonDetId Geometry/CommonDetUnit RecoMuon/DetLayers rootcore rootgraphics root CommonTools/UtilAlgos DataFormats/RPCDigi Geometry/RPCGeometry
HSCPAnalysisHSCPTriggerAuto_PRE_INIT_FUNC += $$(eval $$(call edmPlugin,HSCPAnalysisHSCPTriggerAuto,HSCPAnalysisHSCPTriggerAuto,$(SCRAMSTORENAME_LIB),src/HSCPAnalysis/HSCPTrigger/plugins))
HSCPAnalysisHSCPTriggerAuto_PACKAGE := self/src/HSCPAnalysis/HSCPTrigger/plugins
ALL_PRODS += HSCPAnalysisHSCPTriggerAuto
HSCPAnalysis/HSCPTrigger_forbigobj+=HSCPAnalysisHSCPTriggerAuto
HSCPAnalysisHSCPTriggerAuto_INIT_FUNC        += $$(eval $$(call Library,HSCPAnalysisHSCPTriggerAuto,src/HSCPAnalysis/HSCPTrigger/plugins,src_HSCPAnalysis_HSCPTrigger_plugins,$(SCRAMSTORENAME_BIN),,$(SCRAMSTORENAME_LIB),$(SCRAMSTORENAME_LOGS),edm))
HSCPAnalysisHSCPTriggerAuto_CLASS := LIBRARY
else
$(eval $(call MultipleWarningMsg,HSCPAnalysisHSCPTriggerAuto,src/HSCPAnalysis/HSCPTrigger/plugins))
endif
ALL_COMMONRULES += src_HSCPAnalysis_HSCPTrigger_plugins
src_HSCPAnalysis_HSCPTrigger_plugins_parent := HSCPAnalysis/HSCPTrigger
src_HSCPAnalysis_HSCPTrigger_plugins_INIT_FUNC += $$(eval $$(call CommonProductRules,src_HSCPAnalysis_HSCPTrigger_plugins,src/HSCPAnalysis/HSCPTrigger/plugins,PLUGINS))
ifeq ($(strip $(HSCPAnalysismuSimHitsAuto)),)
HSCPAnalysismuSimHitsAuto := self/src/HSCPAnalysis/muSimHits/plugins
PLUGINS:=yes
HSCPAnalysismuSimHitsAuto_files := $(patsubst src/HSCPAnalysis/muSimHits/plugins/%,%,$(wildcard $(foreach dir,src/HSCPAnalysis/muSimHits/plugins ,$(foreach ext,$(SRC_FILES_SUFFIXES),$(dir)/*.$(ext)))))
HSCPAnalysismuSimHitsAuto_BuildFile    := $(WORKINGDIR)/cache/bf/src/HSCPAnalysis/muSimHits/plugins/BuildFile
HSCPAnalysismuSimHitsAuto_LOC_USE := self   FWCore/Framework FWCore/PluginManager FWCore/ParameterSet DataFormats/DetId DataFormats/MuonDetId RecoMuon/DetLayers
HSCPAnalysismuSimHitsAuto_PRE_INIT_FUNC += $$(eval $$(call edmPlugin,HSCPAnalysismuSimHitsAuto,HSCPAnalysismuSimHitsAuto,$(SCRAMSTORENAME_LIB),src/HSCPAnalysis/muSimHits/plugins))
HSCPAnalysismuSimHitsAuto_PACKAGE := self/src/HSCPAnalysis/muSimHits/plugins
ALL_PRODS += HSCPAnalysismuSimHitsAuto
HSCPAnalysis/muSimHits_forbigobj+=HSCPAnalysismuSimHitsAuto
HSCPAnalysismuSimHitsAuto_INIT_FUNC        += $$(eval $$(call Library,HSCPAnalysismuSimHitsAuto,src/HSCPAnalysis/muSimHits/plugins,src_HSCPAnalysis_muSimHits_plugins,$(SCRAMSTORENAME_BIN),,$(SCRAMSTORENAME_LIB),$(SCRAMSTORENAME_LOGS),edm))
HSCPAnalysismuSimHitsAuto_CLASS := LIBRARY
else
$(eval $(call MultipleWarningMsg,HSCPAnalysismuSimHitsAuto,src/HSCPAnalysis/muSimHits/plugins))
endif
ALL_COMMONRULES += src_HSCPAnalysis_muSimHits_plugins
src_HSCPAnalysis_muSimHits_plugins_parent := HSCPAnalysis/muSimHits
src_HSCPAnalysis_muSimHits_plugins_INIT_FUNC += $$(eval $$(call CommonProductRules,src_HSCPAnalysis_muSimHits_plugins,src/HSCPAnalysis/muSimHits/plugins,PLUGINS))
