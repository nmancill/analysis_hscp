############## All Tools Variables ################
TOOLS_OVERRIDABLE_FLAGS:=
ALL_LIB_TYPES:=
CUDA_TYPE_COMPILER        := cuda
CUDASRC_FILES_SUFFIXES := cu
CXXSRC_FILES_SUFFIXES     := cc cpp cxx C
CSRC_FILES_SUFFIXES       := c
FORTRANSRC_FILES_SUFFIXES := f f77 F F77
SRC_FILES_SUFFIXES        := $(CXXSRC_FILES_SUFFIXES) $(CSRC_FILES_SUFFIXES) $(FORTRANSRC_FILES_SUFFIXES) $(CUDASRC_FILES_SUFFIXES)
SCRAM_ADMIN_DIR           := .SCRAM/$(SCRAM_ARCH)
SCRAM_TOOLS_DIR           := $(SCRAM_ADMIN_DIR)/tools
SCRAM_SCRIPT_EXT          := .py
CFLAGS:=
LIBRARY_CFLAGS:=
TEST_CFLAGS:=
BINARY_CFLAGS:=
EDM_CFLAGS:=
LCGDICT_CFLAGS:=
ROOTDICT_CFLAGS:=
PRECOMPILE_CFLAGS:=
DEV_CFLAGS:=
RELEASE_CFLAGS:=
REM_CFLAGS:=
REM_LIBRARY_CFLAGS:=
REM_TEST_CFLAGS:=
REM_BINARY_CFLAGS:=
REM_EDM_CFLAGS:=
REM_LCGDICT_CFLAGS:=
REM_ROOTDICT_CFLAGS:=
REM_PRECOMPILE_CFLAGS:=
REM_DEV_CFLAGS:=
REM_RELEASE_CFLAGS:=
CPPDEFINES:=
LIBRARY_CPPDEFINES:=
TEST_CPPDEFINES:=
BINARY_CPPDEFINES:=
EDM_CPPDEFINES:=
LCGDICT_CPPDEFINES:=
ROOTDICT_CPPDEFINES:=
PRECOMPILE_CPPDEFINES:=
DEV_CPPDEFINES:=
RELEASE_CPPDEFINES:=
REM_CPPDEFINES:=
REM_LIBRARY_CPPDEFINES:=
REM_TEST_CPPDEFINES:=
REM_BINARY_CPPDEFINES:=
REM_EDM_CPPDEFINES:=
REM_LCGDICT_CPPDEFINES:=
REM_ROOTDICT_CPPDEFINES:=
REM_PRECOMPILE_CPPDEFINES:=
REM_DEV_CPPDEFINES:=
REM_RELEASE_CPPDEFINES:=
CPPFLAGS:=
LIBRARY_CPPFLAGS:=
TEST_CPPFLAGS:=
BINARY_CPPFLAGS:=
EDM_CPPFLAGS:=
LCGDICT_CPPFLAGS:=
ROOTDICT_CPPFLAGS:=
PRECOMPILE_CPPFLAGS:=
DEV_CPPFLAGS:=
RELEASE_CPPFLAGS:=
REM_CPPFLAGS:=
REM_LIBRARY_CPPFLAGS:=
REM_TEST_CPPFLAGS:=
REM_BINARY_CPPFLAGS:=
REM_EDM_CPPFLAGS:=
REM_LCGDICT_CPPFLAGS:=
REM_ROOTDICT_CPPFLAGS:=
REM_PRECOMPILE_CPPFLAGS:=
REM_DEV_CPPFLAGS:=
REM_RELEASE_CPPFLAGS:=
CSHAREDOBJECTFLAGS:=
LIBRARY_CSHAREDOBJECTFLAGS:=
TEST_CSHAREDOBJECTFLAGS:=
BINARY_CSHAREDOBJECTFLAGS:=
EDM_CSHAREDOBJECTFLAGS:=
LCGDICT_CSHAREDOBJECTFLAGS:=
ROOTDICT_CSHAREDOBJECTFLAGS:=
PRECOMPILE_CSHAREDOBJECTFLAGS:=
DEV_CSHAREDOBJECTFLAGS:=
RELEASE_CSHAREDOBJECTFLAGS:=
REM_CSHAREDOBJECTFLAGS:=
REM_LIBRARY_CSHAREDOBJECTFLAGS:=
REM_TEST_CSHAREDOBJECTFLAGS:=
REM_BINARY_CSHAREDOBJECTFLAGS:=
REM_EDM_CSHAREDOBJECTFLAGS:=
REM_LCGDICT_CSHAREDOBJECTFLAGS:=
REM_ROOTDICT_CSHAREDOBJECTFLAGS:=
REM_PRECOMPILE_CSHAREDOBJECTFLAGS:=
REM_DEV_CSHAREDOBJECTFLAGS:=
REM_RELEASE_CSHAREDOBJECTFLAGS:=
CUDA_FLAGS:=
LIBRARY_CUDA_FLAGS:=
TEST_CUDA_FLAGS:=
BINARY_CUDA_FLAGS:=
EDM_CUDA_FLAGS:=
LCGDICT_CUDA_FLAGS:=
ROOTDICT_CUDA_FLAGS:=
PRECOMPILE_CUDA_FLAGS:=
DEV_CUDA_FLAGS:=
RELEASE_CUDA_FLAGS:=
REM_CUDA_FLAGS:=
REM_LIBRARY_CUDA_FLAGS:=
REM_TEST_CUDA_FLAGS:=
REM_BINARY_CUDA_FLAGS:=
REM_EDM_CUDA_FLAGS:=
REM_LCGDICT_CUDA_FLAGS:=
REM_ROOTDICT_CUDA_FLAGS:=
REM_PRECOMPILE_CUDA_FLAGS:=
REM_DEV_CUDA_FLAGS:=
REM_RELEASE_CUDA_FLAGS:=
CUDA_LDFLAGS:=
LIBRARY_CUDA_LDFLAGS:=
TEST_CUDA_LDFLAGS:=
BINARY_CUDA_LDFLAGS:=
EDM_CUDA_LDFLAGS:=
LCGDICT_CUDA_LDFLAGS:=
ROOTDICT_CUDA_LDFLAGS:=
PRECOMPILE_CUDA_LDFLAGS:=
DEV_CUDA_LDFLAGS:=
RELEASE_CUDA_LDFLAGS:=
REM_CUDA_LDFLAGS:=
REM_LIBRARY_CUDA_LDFLAGS:=
REM_TEST_CUDA_LDFLAGS:=
REM_BINARY_CUDA_LDFLAGS:=
REM_EDM_CUDA_LDFLAGS:=
REM_LCGDICT_CUDA_LDFLAGS:=
REM_ROOTDICT_CUDA_LDFLAGS:=
REM_PRECOMPILE_CUDA_LDFLAGS:=
REM_DEV_CUDA_LDFLAGS:=
REM_RELEASE_CUDA_LDFLAGS:=
CXXFLAGS:=
LIBRARY_CXXFLAGS:=
TEST_CXXFLAGS:=
BINARY_CXXFLAGS:=
EDM_CXXFLAGS:=
LCGDICT_CXXFLAGS:=
ROOTDICT_CXXFLAGS:=
PRECOMPILE_CXXFLAGS:=
DEV_CXXFLAGS:=
RELEASE_CXXFLAGS:=
REM_CXXFLAGS:=
REM_LIBRARY_CXXFLAGS:=
REM_TEST_CXXFLAGS:=
REM_BINARY_CXXFLAGS:=
REM_EDM_CXXFLAGS:=
REM_LCGDICT_CXXFLAGS:=
REM_ROOTDICT_CXXFLAGS:=
REM_PRECOMPILE_CXXFLAGS:=
REM_DEV_CXXFLAGS:=
REM_RELEASE_CXXFLAGS:=
CXXSHAREDFLAGS:=
LIBRARY_CXXSHAREDFLAGS:=
TEST_CXXSHAREDFLAGS:=
BINARY_CXXSHAREDFLAGS:=
EDM_CXXSHAREDFLAGS:=
LCGDICT_CXXSHAREDFLAGS:=
ROOTDICT_CXXSHAREDFLAGS:=
PRECOMPILE_CXXSHAREDFLAGS:=
DEV_CXXSHAREDFLAGS:=
RELEASE_CXXSHAREDFLAGS:=
REM_CXXSHAREDFLAGS:=
REM_LIBRARY_CXXSHAREDFLAGS:=
REM_TEST_CXXSHAREDFLAGS:=
REM_BINARY_CXXSHAREDFLAGS:=
REM_EDM_CXXSHAREDFLAGS:=
REM_LCGDICT_CXXSHAREDFLAGS:=
REM_ROOTDICT_CXXSHAREDFLAGS:=
REM_PRECOMPILE_CXXSHAREDFLAGS:=
REM_DEV_CXXSHAREDFLAGS:=
REM_RELEASE_CXXSHAREDFLAGS:=
CXXSHAREDOBJECTFLAGS:=
LIBRARY_CXXSHAREDOBJECTFLAGS:=
TEST_CXXSHAREDOBJECTFLAGS:=
BINARY_CXXSHAREDOBJECTFLAGS:=
EDM_CXXSHAREDOBJECTFLAGS:=
LCGDICT_CXXSHAREDOBJECTFLAGS:=
ROOTDICT_CXXSHAREDOBJECTFLAGS:=
PRECOMPILE_CXXSHAREDOBJECTFLAGS:=
DEV_CXXSHAREDOBJECTFLAGS:=
RELEASE_CXXSHAREDOBJECTFLAGS:=
REM_CXXSHAREDOBJECTFLAGS:=
REM_LIBRARY_CXXSHAREDOBJECTFLAGS:=
REM_TEST_CXXSHAREDOBJECTFLAGS:=
REM_BINARY_CXXSHAREDOBJECTFLAGS:=
REM_EDM_CXXSHAREDOBJECTFLAGS:=
REM_LCGDICT_CXXSHAREDOBJECTFLAGS:=
REM_ROOTDICT_CXXSHAREDOBJECTFLAGS:=
REM_PRECOMPILE_CXXSHAREDOBJECTFLAGS:=
REM_DEV_CXXSHAREDOBJECTFLAGS:=
REM_RELEASE_CXXSHAREDOBJECTFLAGS:=
FFLAGS:=
LIBRARY_FFLAGS:=
TEST_FFLAGS:=
BINARY_FFLAGS:=
EDM_FFLAGS:=
LCGDICT_FFLAGS:=
ROOTDICT_FFLAGS:=
PRECOMPILE_FFLAGS:=
DEV_FFLAGS:=
RELEASE_FFLAGS:=
REM_FFLAGS:=
REM_LIBRARY_FFLAGS:=
REM_TEST_FFLAGS:=
REM_BINARY_FFLAGS:=
REM_EDM_FFLAGS:=
REM_LCGDICT_FFLAGS:=
REM_ROOTDICT_FFLAGS:=
REM_PRECOMPILE_FFLAGS:=
REM_DEV_FFLAGS:=
REM_RELEASE_FFLAGS:=
FOPTIMISEDFLAGS:=
LIBRARY_FOPTIMISEDFLAGS:=
TEST_FOPTIMISEDFLAGS:=
BINARY_FOPTIMISEDFLAGS:=
EDM_FOPTIMISEDFLAGS:=
LCGDICT_FOPTIMISEDFLAGS:=
ROOTDICT_FOPTIMISEDFLAGS:=
PRECOMPILE_FOPTIMISEDFLAGS:=
DEV_FOPTIMISEDFLAGS:=
RELEASE_FOPTIMISEDFLAGS:=
REM_FOPTIMISEDFLAGS:=
REM_LIBRARY_FOPTIMISEDFLAGS:=
REM_TEST_FOPTIMISEDFLAGS:=
REM_BINARY_FOPTIMISEDFLAGS:=
REM_EDM_FOPTIMISEDFLAGS:=
REM_LCGDICT_FOPTIMISEDFLAGS:=
REM_ROOTDICT_FOPTIMISEDFLAGS:=
REM_PRECOMPILE_FOPTIMISEDFLAGS:=
REM_DEV_FOPTIMISEDFLAGS:=
REM_RELEASE_FOPTIMISEDFLAGS:=
FSHAREDOBJECTFLAGS:=
LIBRARY_FSHAREDOBJECTFLAGS:=
TEST_FSHAREDOBJECTFLAGS:=
BINARY_FSHAREDOBJECTFLAGS:=
EDM_FSHAREDOBJECTFLAGS:=
LCGDICT_FSHAREDOBJECTFLAGS:=
ROOTDICT_FSHAREDOBJECTFLAGS:=
PRECOMPILE_FSHAREDOBJECTFLAGS:=
DEV_FSHAREDOBJECTFLAGS:=
RELEASE_FSHAREDOBJECTFLAGS:=
REM_FSHAREDOBJECTFLAGS:=
REM_LIBRARY_FSHAREDOBJECTFLAGS:=
REM_TEST_FSHAREDOBJECTFLAGS:=
REM_BINARY_FSHAREDOBJECTFLAGS:=
REM_EDM_FSHAREDOBJECTFLAGS:=
REM_LCGDICT_FSHAREDOBJECTFLAGS:=
REM_ROOTDICT_FSHAREDOBJECTFLAGS:=
REM_PRECOMPILE_FSHAREDOBJECTFLAGS:=
REM_DEV_FSHAREDOBJECTFLAGS:=
REM_RELEASE_FSHAREDOBJECTFLAGS:=
LDFLAGS:=
LIBRARY_LDFLAGS:=
TEST_LDFLAGS:=
BINARY_LDFLAGS:=
EDM_LDFLAGS:=
LCGDICT_LDFLAGS:=
ROOTDICT_LDFLAGS:=
PRECOMPILE_LDFLAGS:=
DEV_LDFLAGS:=
RELEASE_LDFLAGS:=
REM_LDFLAGS:=
REM_LIBRARY_LDFLAGS:=
REM_TEST_LDFLAGS:=
REM_BINARY_LDFLAGS:=
REM_EDM_LDFLAGS:=
REM_LCGDICT_LDFLAGS:=
REM_ROOTDICT_LDFLAGS:=
REM_PRECOMPILE_LDFLAGS:=
REM_DEV_LDFLAGS:=
REM_RELEASE_LDFLAGS:=
LD_UNIT:=
LIBRARY_LD_UNIT:=
TEST_LD_UNIT:=
BINARY_LD_UNIT:=
EDM_LD_UNIT:=
LCGDICT_LD_UNIT:=
ROOTDICT_LD_UNIT:=
PRECOMPILE_LD_UNIT:=
DEV_LD_UNIT:=
RELEASE_LD_UNIT:=
REM_LD_UNIT:=
REM_LIBRARY_LD_UNIT:=
REM_TEST_LD_UNIT:=
REM_BINARY_LD_UNIT:=
REM_EDM_LD_UNIT:=
REM_LCGDICT_LD_UNIT:=
REM_ROOTDICT_LD_UNIT:=
REM_PRECOMPILE_LD_UNIT:=
REM_DEV_LD_UNIT:=
REM_RELEASE_LD_UNIT:=
ALL_COMPILER_FLAGS := CFLAGS CPPDEFINES CPPFLAGS CSHAREDOBJECTFLAGS CUDA_FLAGS CUDA_LDFLAGS CXXFLAGS CXXSHAREDFLAGS CXXSHAREDOBJECTFLAGS FFLAGS FOPTIMISEDFLAGS FSHAREDOBJECTFLAGS LDFLAGS LD_UNIT 
SCRAM_MULTIPLE_COMPILERS := yes
SCRAM_DEFAULT_COMPILER    := gcc
SCRAM_COMPILER            := $(SCRAM_DEFAULT_COMPILER)
ifdef COMPILER
SCRAM_COMPILER            := $(COMPILER)
endif
CXX_TYPE_COMPILER := cxxcompiler
C_TYPE_COMPILER := ccompiler
F77_TYPE_COMPILER := f77compiler
ifndef SCRAM_IGNORE_MISSING_COMPILERS
$(if $(wildcard $(SCRAM_TOOLS_DIR)/$(SCRAM_COMPILER)-$(CXX_TYPE_COMPILER)),,$(info ****WARNING: You have selected $(SCRAM_COMPILER) as compiler but there is no $(SCRAM_COMPILER)-$(CXX_TYPE_COMPILER) tool setup. Default compiler $(SCRAM_DEFAULT_COMPILER)-$(CXX_TYPE_COMPILER) will be used to compile CXX files))
$(if $(wildcard $(SCRAM_TOOLS_DIR)/$(SCRAM_COMPILER)-$(C_TYPE_COMPILER)),,$(info ****WARNING: You have selected $(SCRAM_COMPILER) as compiler but there is no $(SCRAM_COMPILER)-$(C_TYPE_COMPILER) tool setup. Default compiler $(SCRAM_DEFAULT_COMPILER)-$(C_TYPE_COMPILER) will be used to compile C files))
$(if $(wildcard $(SCRAM_TOOLS_DIR)/$(SCRAM_COMPILER)-$(F77_TYPE_COMPILER)),,$(info ****WARNING: You have selected $(SCRAM_COMPILER) as compiler but there is no $(SCRAM_COMPILER)-$(F77_TYPE_COMPILER) tool setup. Default compiler $(SCRAM_DEFAULT_COMPILER)-$(F77_TYPE_COMPILER) will be used to compile F77 files))
endif
GCC_CXXCOMPILER_BASE:=/cvmfs/cms.cern.ch/slc7_amd64_gcc900/external/gcc/9.3.0
CXX:=/cvmfs/cms.cern.ch/slc7_amd64_gcc900/external/gcc/9.3.0/bin/c++
gcc_CXX:=/cvmfs/cms.cern.ch/slc7_amd64_gcc900/external/gcc/9.3.0/bin/c++
GCC_CCOMPILER_BASE:=/cvmfs/cms.cern.ch/slc7_amd64_gcc900/external/gcc/9.3.0
CC:=/cvmfs/cms.cern.ch/slc7_amd64_gcc900/external/gcc/9.3.0/bin/gcc
gcc_CC:=/cvmfs/cms.cern.ch/slc7_amd64_gcc900/external/gcc/9.3.0/bin/gcc
GCC_F77COMPILER_BASE:=/cvmfs/cms.cern.ch/slc7_amd64_gcc900/external/gcc/9.3.0
FC:=/cvmfs/cms.cern.ch/slc7_amd64_gcc900/external/gcc/9.3.0/bin/gfortran
gcc_FC:=/cvmfs/cms.cern.ch/slc7_amd64_gcc900/external/gcc/9.3.0/bin/gfortran
ALL_TOOLS  += cxxcompiler
cxxcompiler_EX_USE    := $(if $(strip $(wildcard $(LOCALTOP)/$(SCRAM_TOOLS_DIR)/$(SCRAM_COMPILER)-cxxcompiler)),$(SCRAM_COMPILER)-cxxcompiler,$(SCRAM_DEFAULT_COMPILER)-cxxcompiler)
ALL_TOOLS  += ccompiler
ccompiler_EX_USE    := $(if $(strip $(wildcard $(LOCALTOP)/$(SCRAM_TOOLS_DIR)/$(SCRAM_COMPILER)-ccompiler)),$(SCRAM_COMPILER)-ccompiler,$(SCRAM_DEFAULT_COMPILER)-ccompiler)
ALL_TOOLS  += f77compiler
f77compiler_EX_USE    := $(if $(strip $(wildcard $(LOCALTOP)/$(SCRAM_TOOLS_DIR)/$(SCRAM_COMPILER)-f77compiler)),$(SCRAM_COMPILER)-f77compiler,$(SCRAM_DEFAULT_COMPILER)-f77compiler)
CPPUNIT_BASE:=/cvmfs/cms.cern.ch/slc7_amd64_gcc900/external/cppunit/1.15.x-cms
MPFR_BASE:=/cvmfs/cms.cern.ch/slc7_amd64_gcc900/external/mpfr-static/4.0.2-cms
GBL_BASE:=/cvmfs/cms.cern.ch/slc7_amd64_gcc900/external/gbl/V02-01-03-llifpc2
HYDJET_BASE:=/cvmfs/cms.cern.ch/slc7_amd64_gcc900/external/hydjet/1.9.1-llifpc2
HEPPDT_BASE:=/cvmfs/cms.cern.ch/slc7_amd64_gcc900/external/heppdt/3.04.01-727b82841391596ccf52f688a9ff66d7
CMSSWDATA_BASE:=/cvmfs/cms.cern.ch/slc7_amd64_gcc900/cms
CMSSW_DATA_PATH:=/cvmfs/cms.cern.ch/slc7_amd64_gcc900/cms
OPENBLAS_BASE:=/cvmfs/cms.cern.ch/slc7_amd64_gcc900/external/OpenBLAS/0.3.15
LIBXSLT_BASE:=/cvmfs/cms.cern.ch/slc7_amd64_gcc900/external/libxslt/1.1.28-llifpc
PHOTOS_BASE:=/cvmfs/cms.cern.ch/slc7_amd64_gcc900/external/photos/215.5-bcolbf
LIBJPEG_TURBO_BASE:=/cvmfs/cms.cern.ch/slc7_amd64_gcc900/external/libjpeg-turbo/2.0.2-llifpc
GCC_PLUGIN_BASE:=/cvmfs/cms.cern.ch/slc7_amd64_gcc900/external/gcc/9.3.0/bin/../lib/gcc/x86_64-unknown-linux-gnu/9.3.0/plugin
PY3_AUTOPEP8_BASE:=/cvmfs/cms.cern.ch/slc7_amd64_gcc900/external/py3-autopep8/1.5.5-llifpc3
LZ4_BASE:=/cvmfs/cms.cern.ch/slc7_amd64_gcc900/external/lz4/1.9.2
JIMMY_HEADERS_BASE:=/cvmfs/cms.cern.ch/slc7_amd64_gcc900/external/jimmy/4.2-llifpc2
PY3_HISTOGRAMMAR_BASE:=/cvmfs/cms.cern.ch/slc7_amd64_gcc900/external/py3-histogrammar/1.0.10-llifpc
PY3_HYPEROPT_BASE:=/cvmfs/cms.cern.ch/slc7_amd64_gcc900/external/py3-hyperopt/0.2.5-llifpc2
UTM_BASE:=/cvmfs/cms.cern.ch/slc7_amd64_gcc900/external/utm/utm_0.9.1-5bd8778a3da93a7b7de7a4c1a527fdb8
DB6_BASE:=/cvmfs/cms.cern.ch/slc7_amd64_gcc900/external/db6/6.2.32-bcolbf
ifeq ($(strip $(SCRAM_COMPILER)),llvm)
FC:=/cvmfs/cms.cern.ch/slc7_amd64_gcc900/external/gcc/9.3.0/bin/gfortran
endif
llvm_FC:=/cvmfs/cms.cern.ch/slc7_amd64_gcc900/external/gcc/9.3.0/bin/gfortran
MAKE_BASE:=/cvmfs/cms.cern.ch/slc7_amd64_gcc900/external/gmake/4.3
PY3_DXR_BASE:=/cvmfs/cms.cern.ch/slc7_amd64_gcc900/external/py3-dxr/1.0.x-938568265a5bb288a690f368fde63fb0
JIMMY_BASE:=/cvmfs/cms.cern.ch/slc7_amd64_gcc900/external/jimmy/4.2-llifpc2
FASTJET_CONTRIB_BASE:=/cvmfs/cms.cern.ch/slc7_amd64_gcc900/external/fastjet-contrib/1.044-llifpc3
GOOGLE_BENCHMARK_BASE:=/cvmfs/cms.cern.ch/slc7_amd64_gcc900/external/google-benchmark/1.4.x-llifpc2
BLACKHAT_BASE:=/cvmfs/cms.cern.ch/slc7_amd64_gcc900/external/blackhat/0.9.9-llifpc2
GSL_BASE:=/cvmfs/cms.cern.ch/slc7_amd64_gcc900/external/gsl/2.6-llifpc
BZ2LIB_BASE:=/cvmfs/cms.cern.ch/slc7_amd64_gcc900/external/bz2lib/1.0.6-bcolbf2
HIGHFIVE_BASE:=/cvmfs/cms.cern.ch/slc7_amd64_gcc900/external/highfive/2.0-a9b710ef0623387d1c81ccf51b8c9d2f
PY3_LIZARD_BASE:=/cvmfs/cms.cern.ch/slc7_amd64_gcc900/external/py3-lizard/1.17.7-llifpc
PY3_GOOGLE_AUTH_OAUTHLIB_BASE:=/cvmfs/cms.cern.ch/slc7_amd64_gcc900/external/py3-google-auth-oauthlib/0.4.2-llifpc3
FFTJET_BASE:=/cvmfs/cms.cern.ch/slc7_amd64_gcc900/external/fftjet/1.5.0-llifpc
PY3_LAW_BASE:=/cvmfs/cms.cern.ch/slc7_amd64_gcc900/external/py3-law/0.1.3-llifpc3
OPENCV_BASE:=/cvmfs/cms.cern.ch/slc7_amd64_gcc900/external/opencv/4.5.1-llifpc3
JSON_BASE:=/cvmfs/cms.cern.ch/slc7_amd64_gcc900/external/json/3.7.3
JEMALLOC_BASE:=/cvmfs/cms.cern.ch/slc7_amd64_gcc900/external/jemalloc/5.2.1-cms
GRPC_BASE:=/cvmfs/cms.cern.ch/slc7_amd64_gcc900/external/grpc/1.35.0-llifpc2
GDRCOPY_BASE:=/cvmfs/cms.cern.ch/slc7_amd64_gcc900/external/gdrcopy/2.2-99bcd757ab5a91e1c01839bacbb25013
BOOSTHEADER_BASE:=/cvmfs/cms.cern.ch/slc7_amd64_gcc900/external/boost/1.75.0-a4dc570b8c1b0e1997e4bd60f590bedd
FRONTIER_CLIENT_BASE:=/cvmfs/cms.cern.ch/slc7_amd64_gcc900/external/frontier_client/2.9.1-llifpc2
GNUPLOT_BASE:=/cvmfs/cms.cern.ch/slc7_amd64_gcc900/external/gnuplot/4.6.5-bcolbf
FFTW3_BASE:=/cvmfs/cms.cern.ch/slc7_amd64_gcc900/external/fftw3/3.3.8-bcolbf
XTL_BASE:=/cvmfs/cms.cern.ch/slc7_amd64_gcc900/external/xtl/0.6.3-llifpc
HEPMC_BASE:=/cvmfs/cms.cern.ch/slc7_amd64_gcc900/external/hepmc/2.06.10-llifpc
HEPMC_HEADERS_BASE:=/cvmfs/cms.cern.ch/slc7_amd64_gcc900/external/hepmc/2.06.10-llifpc
TBB_BASE:=/cvmfs/cms.cern.ch/slc7_amd64_gcc900/external/tbb/v2021.2.0-668f9dc16735894655a062ddf70e63db
PYTHON_BASE:=/cvmfs/cms.cern.ch/slc7_amd64_gcc900/external/python/2.7.15-cms
PYTHON_COMPILE:=/cvmfs/cms.cern.ch/slc7_amd64_gcc900/external/python/2.7.15-cms/lib/python2.7/compileall.py
TOPREX_HEADERS_BASE:=/cvmfs/cms.cern.ch/slc7_amd64_gcc900/external/toprex/4.23-ghbfee
VECGEOM_INTERFACE_BASE:=/cvmfs/cms.cern.ch/slc7_amd64_gcc900/external/vecgeom/v1.1.16-llifpc
XTENSOR_BASE:=/cvmfs/cms.cern.ch/slc7_amd64_gcc900/external/xtensor/0.20.1-llifpc
PY3_PIP_BASE:=/cvmfs/cms.cern.ch/slc7_amd64_gcc900/external/py3-pip/20.3.3-llifpc
STARLIGHT_BASE:=/cvmfs/cms.cern.ch/slc7_amd64_gcc900/external/starlight/r193-llifpc2
PY3_VIRTUALENV_CLONE_BASE:=/cvmfs/cms.cern.ch/slc7_amd64_gcc900/external/py3-virtualenv-clone/0.5.4-llifpc
TCMALLOC_MINIMAL_BASE:=/cvmfs/cms.cern.ch/slc7_amd64_gcc900/external/gperftools/2.9.1-cms
PY3_NUMBA_BASE:=/cvmfs/cms.cern.ch/slc7_amd64_gcc900/external/py3-numba/0.53.0-5e1d0c8dbe1dd32ce754bead4ae20b9f
PY3_FLAWFINDER_BASE:=/cvmfs/cms.cern.ch/slc7_amd64_gcc900/external/py3-flawfinder/2.0.15-llifpc
PY3_TABLES_BASE:=/cvmfs/cms.cern.ch/slc7_amd64_gcc900/external/py3-tables/3.6.1-32d93702dc85438299e3306867f9012c
GEANT4STATIC_BASE:=/cvmfs/cms.cern.ch/slc7_amd64_gcc900/external/geant4/10.7.1-llifpc3
GOSAMCONTRIB_BASE:=/cvmfs/cms.cern.ch/slc7_amd64_gcc900/external/gosamcontrib/2.0-20150803-bcolbf2
GDBM_BASE:=/cvmfs/cms.cern.ch/slc7_amd64_gcc900/external/gdbm/1.10-bcolbf2
CGAL_BASE:=/cvmfs/cms.cern.ch/slc7_amd64_gcc900/external/cgal/4.2-b35f3b6b214e0bb5d088ec59db1635eb
ifeq ($(strip $(SCRAM_COMPILER)),iwyu)
LLVM_CXXCOMPILER_BASE:=/cvmfs/cms.cern.ch/slc7_amd64_gcc900/external/llvm/12.0.0-dba5ec8511dcec4cd8ee2b03d956f9be
CXX:=/cvmfs/cms.cern.ch/slc7_amd64_gcc900/external/llvm/12.0.0-dba5ec8511dcec4cd8ee2b03d956f9be/bin/include-what-you-use
endif
iwyu_LLVM_CXXCOMPILER_BASE:=/cvmfs/cms.cern.ch/slc7_amd64_gcc900/external/llvm/12.0.0-dba5ec8511dcec4cd8ee2b03d956f9be
iwyu_CXX:=/cvmfs/cms.cern.ch/slc7_amd64_gcc900/external/llvm/12.0.0-dba5ec8511dcec4cd8ee2b03d956f9be/bin/include-what-you-use
HDF5_BASE:=/cvmfs/cms.cern.ch/slc7_amd64_gcc900/external/hdf5/1.10.6-199c971d11482ea7c5b698cf59154d6e
PY3_COVERAGE_BASE:=/cvmfs/cms.cern.ch/slc7_amd64_gcc900/external/py3-coverage/5.5-llifpc
CUPLA_BASE:=/cvmfs/cms.cern.ch/slc7_amd64_gcc900/external/cupla/0.3.0-dev-0740f3db9af195edb111f5a2380d2b40
XZ_BASE:=/cvmfs/cms.cern.ch/slc7_amd64_gcc900/external/xz/5.2.4-cms
GLIMPSE_BASE:=/cvmfs/cms.cern.ch/slc7_amd64_gcc900/external/glimpse/4.18.5-cms
HWLOC_BASE:=/cvmfs/cms.cern.ch/slc7_amd64_gcc900/external/hwloc/2.4.0-2dd3165601ce88b8273a7a2713da07c8
DAVIX_BASE:=/cvmfs/cms.cern.ch/slc7_amd64_gcc900/external/davix/0.7.6-llifpc
BOOST_PYTHON_BASE:=/cvmfs/cms.cern.ch/slc7_amd64_gcc900/external/boost/1.75.0-a4dc570b8c1b0e1997e4bd60f590bedd
CUDA_COMPATIBLE_RUNTIME_BASE:=/cvmfs/cms.cern.ch/slc7_amd64_gcc900/external/cuda-compatible-runtime/1.0-5963b5c737af2dac1d8c566050b38748
ALPAKA_BASE:=/cvmfs/cms.cern.ch/slc7_amd64_gcc900/external/alpaka/0.6.0-a35f3d51edfa3d4c7d328f0cd95e56fe
CLHEPHEADER_BASE:=/cvmfs/cms.cern.ch/slc7_amd64_gcc900/external/clhep/2.4.4.0-llifpc2
PACPARSER_BASE:=/cvmfs/cms.cern.ch/slc7_amd64_gcc900/external/pacparser/1.3.5-bcolbf
LIBXML2_BASE:=/cvmfs/cms.cern.ch/slc7_amd64_gcc900/external/libxml2/2.9.10-cms
LIBFFI_BASE:=/cvmfs/cms.cern.ch/slc7_amd64_gcc900/external/libffi/3.2.1-bcolbf
EVTGEN_BASE:=/cvmfs/cms.cern.ch/slc7_amd64_gcc900/external/evtgen/2.0.0-llifpc2
MXNET_PREDICT_BASE:=/cvmfs/cms.cern.ch/slc7_amd64_gcc900/external/mxnet-predict/1.6.0-llifpc3
OPENMPI_BASE:=/cvmfs/cms.cern.ch/slc7_amd64_gcc900/external/openmpi/4.1.0-3f327f5f063af0a2724fcf94518cd985
PCRE_BASE:=/cvmfs/cms.cern.ch/slc7_amd64_gcc900/external/pcre/8.43-bcolbf
GEANT4CORE_BASE:=/cvmfs/cms.cern.ch/slc7_amd64_gcc900/external/geant4/10.7.1-llifpc3
G4LIB:=/cvmfs/cms.cern.ch/slc7_amd64_gcc900/external/geant4/10.7.1-llifpc3/lib
UCX_BASE:=/cvmfs/cms.cern.ch/slc7_amd64_gcc900/external/ucx/1.9.0-5c6c7a906fc602eeb64276b7a691062d
CURL_BASE:=/cvmfs/cms.cern.ch/slc7_amd64_gcc900/external/curl/7.70.0-cms
LLVM_CCOMPILER_BASE:=/cvmfs/cms.cern.ch/slc7_amd64_gcc900/external/llvm/12.0.0-dba5ec8511dcec4cd8ee2b03d956f9be
ifeq ($(strip $(SCRAM_COMPILER)),llvm)
CC:=/cvmfs/cms.cern.ch/slc7_amd64_gcc900/external/llvm/12.0.0-dba5ec8511dcec4cd8ee2b03d956f9be/bin/clang
endif
llvm_CC:=/cvmfs/cms.cern.ch/slc7_amd64_gcc900/external/llvm/12.0.0-dba5ec8511dcec4cd8ee2b03d956f9be/bin/clang
GIT_BASE:=/cvmfs/cms.cern.ch/slc7_amd64_gcc900/external/git/2.23.0-cms
LLVM_CXXCOMPILER_BASE:=/cvmfs/cms.cern.ch/slc7_amd64_gcc900/external/llvm/12.0.0-dba5ec8511dcec4cd8ee2b03d956f9be
ifeq ($(strip $(SCRAM_COMPILER)),llvm)
CXX:=/cvmfs/cms.cern.ch/slc7_amd64_gcc900/external/llvm/12.0.0-dba5ec8511dcec4cd8ee2b03d956f9be/bin/clang++
endif
llvm_CXX:=/cvmfs/cms.cern.ch/slc7_amd64_gcc900/external/llvm/12.0.0-dba5ec8511dcec4cd8ee2b03d956f9be/bin/clang++
MESCHACH_BASE:=/cvmfs/cms.cern.ch/slc7_amd64_gcc900/external/meschach/1.2.pCMS1-bcolbf2
CLASSLIB_BASE:=/cvmfs/cms.cern.ch/slc7_amd64_gcc900/external/classlib/3.1.3-fa415b2061ef54fdf7db6d6fb07f0a50
PY3_CYTHON_BASE:=/cvmfs/cms.cern.ch/slc7_amd64_gcc900/external/py3-cython/0.29.23-llifpc
EIGEN_BASE:=/cvmfs/cms.cern.ch/slc7_amd64_gcc900/external/eigen/f612df273689a19d25b45ca4f8269463207c4fee
ZSTD_BASE:=/cvmfs/cms.cern.ch/slc7_amd64_gcc900/external/zstd/1.4.5-llifpc
PROFESSOR2_BASE:=/cvmfs/cms.cern.ch/slc7_amd64_gcc900/external/professor2/2.3.2-9685966a96ccf5148d401447fff5f3e6
PY3_JUPYTER_CONSOLE_BASE:=/cvmfs/cms.cern.ch/slc7_amd64_gcc900/external/py3-jupyter_console/6.2.0-llifpc4
PY3_CACHECONTROL_BASE:=/cvmfs/cms.cern.ch/slc7_amd64_gcc900/external/py3-cachecontrol/0.12.6-llifpc
LIBPCIACCESS_BASE:=/cvmfs/cms.cern.ch/slc7_amd64_gcc900/external/libpciaccess/0.16-cms
HLS_BASE:=/cvmfs/cms.cern.ch/slc7_amd64_gcc900/external/hls/2019.08-bcolbf
LIBPNG_BASE:=/cvmfs/cms.cern.ch/slc7_amd64_gcc900/external/libpng/1.6.37-cms
HERWIG_BASE:=/cvmfs/cms.cern.ch/slc7_amd64_gcc900/external/herwig/6.521-llifpc2
ITTNOTIFY_BASE:=/cvmfs/cms.cern.ch/slc7_amd64_gcc900/external/ittnotify/16.06.18-llifpc
CATCH2_BASE:=/cvmfs/cms.cern.ch/slc7_amd64_gcc900/external/catch2/2.13.6
GOSAM_BASE:=/cvmfs/cms.cern.ch/slc7_amd64_gcc900/external/gosam/2.1.0-llifpc
FASTJET_BASE:=/cvmfs/cms.cern.ch/slc7_amd64_gcc900/external/fastjet/3.4.0
LLVM_BASE:=/cvmfs/cms.cern.ch/slc7_amd64_gcc900/external/llvm/12.0.0-dba5ec8511dcec4cd8ee2b03d956f9be
LIBUNWIND_BASE:=/cvmfs/cms.cern.ch/slc7_amd64_gcc900/external/libunwind/1.5-cms
PY3_JUPYTER_CLIENT_BASE:=/cvmfs/cms.cern.ch/slc7_amd64_gcc900/external/py3-jupyter_client/6.1.11-llifpc4
PY3_FLAKE8_BASE:=/cvmfs/cms.cern.ch/slc7_amd64_gcc900/external/py3-flake8/3.8.4-llifpc3
MADGRAPH5AMCATNLO_BASE:=/cvmfs/cms.cern.ch/slc7_amd64_gcc900/external/madgraph5amcatnlo/2.7.3-85c32c0b3ebdcb844589da4bc8f9d649
MKFITBASE:=/cvmfs/cms.cern.ch/slc7_amd64_gcc900/external/mkfit/3.1.0-0f49d05aa3fbd1ebf7cafab9ef474019
NUMPY_C_API_BASE:=/cvmfs/cms.cern.ch/slc7_amd64_gcc900/external/py3-numpy/1.17.5-llifpc3
ONNXRUNTIME_BASE:=/cvmfs/cms.cern.ch/slc7_amd64_gcc900/external/onnxruntime/1.7.2-755e50235f49855a855bba79cb61bfea
GRAPHVIZ_BASE:=/cvmfs/cms.cern.ch/slc7_amd64_gcc900/external/graphviz/2.38.0-llifpc
BOOST_BASE:=/cvmfs/cms.cern.ch/slc7_amd64_gcc900/external/boost/1.75.0-a4dc570b8c1b0e1997e4bd60f590bedd
CUDA_BASE:=/cvmfs/cms.cern.ch/slc7_amd64_gcc900/external/cuda/11.2.2-541fb61f7fe80980f444615bcda5231f
NVCC:=/cvmfs/cms.cern.ch/slc7_amd64_gcc900/external/cuda/11.2.2-541fb61f7fe80980f444615bcda5231f/bin/nvcc
PY3_MAKO_BASE:=/cvmfs/cms.cern.ch/slc7_amd64_gcc900/external/py3-Mako/1.1.4-llifpc
FLATBUFFERS_BASE:=/cvmfs/cms.cern.ch/slc7_amd64_gcc900/external/flatbuffers/1.12.0-llifpc
FREETYPE_BASE:=/cvmfs/cms.cern.ch/slc7_amd64_gcc900/external/freetype/2.10.0-cms
GPERF_BASE:=/cvmfs/cms.cern.ch/slc7_amd64_gcc900/external/gperftools/2.9.1-cms
LIBUUID_BASE:=/cvmfs/cms.cern.ch/slc7_amd64_gcc900/external/libuuid/2.34-bcolbf2
CSCTRACKFINDEREMULATION_BASE:=/cvmfs/cms.cern.ch/slc7_amd64_gcc900/cms/CSCTrackFinderEmulation/1.2-bcolbf
NUMACTL_BASE:=/cvmfs/cms.cern.ch/slc7_amd64_gcc900/external/numactl/2.0.14-cms
DABLOOMS_BASE:=/cvmfs/cms.cern.ch/slc7_amd64_gcc900/external/dablooms/0.9.1-bcolbf
GEANT4_PARFULLCMS:=/cvmfs/cms.cern.ch/slc7_amd64_gcc900/external/geant4-parfullcms/2014.01.27-llifpc3
CHARYBDIS_BASE:=/cvmfs/cms.cern.ch/slc7_amd64_gcc900/external/charybdis/1.003-llifpc2
GIFLIB_BASE:=/cvmfs/cms.cern.ch/slc7_amd64_gcc900/external/giflib/5.2.0-cms
CUDNN_BASE:=/cvmfs/cms.cern.ch/slc7_amd64_gcc900/external/cudnn/8.1.1.33-f7b0d1a7632ebd42b2dfc36237d910c4
OPENLDAP_BASE:=/cvmfs/cms.cern.ch/slc7_amd64_gcc900/external/openldap/2.4.45-cms
OPENLOOPS_BASE:=/cvmfs/cms.cern.ch/slc7_amd64_gcc900/external/openloops/2.1.2-llifpc
PY3_QTCONSOLE_BASE:=/cvmfs/cms.cern.ch/slc7_amd64_gcc900/external/py3-qtconsole/4.7.7-llifpc3
PY3_MARKDOWN_BASE:=/cvmfs/cms.cern.ch/slc7_amd64_gcc900/external/py3-Markdown/3.1.1
PY3_RSA_BASE:=/cvmfs/cms.cern.ch/slc7_amd64_gcc900/external/py3-rsa/4.7.2-llifpc2
PY3_PYCODESTYLE_BASE:=/cvmfs/cms.cern.ch/slc7_amd64_gcc900/external/py3-pycodestyle/2.6.0-llifpc
LWTNN_BASE:=/cvmfs/cms.cern.ch/slc7_amd64_gcc900/external/lwtnn/2.10-aa5fb90f77dc8608c15baa76feaa3960
LIBTIFF_BASE:=/cvmfs/cms.cern.ch/slc7_amd64_gcc900/external/libtiff/4.0.10-llifpc
MCTESTER_BASE:=/cvmfs/cms.cern.ch/slc7_amd64_gcc900/external/mctester/1.25.0a-7ea6d41fc955f8d0a76fcc0420d2cd07
GCC_ATOMIC_BASE:=/cvmfs/cms.cern.ch/slc7_amd64_gcc900/external/gcc/9.3.0
CUPTI_BASE:=/cvmfs/cms.cern.ch/slc7_amd64_gcc900/external/cuda/11.2.2-541fb61f7fe80980f444615bcda5231f
CUDA_STUBS_BASE:=/cvmfs/cms.cern.ch/slc7_amd64_gcc900/external/cuda/11.2.2-541fb61f7fe80980f444615bcda5231f
DOXYGEN_BASE:=/cvmfs/cms.cern.ch/slc7_amd64_gcc900/external/doxygen/1.8.16-llifpc2
CASCADE_BASE:=/cvmfs/cms.cern.ch/slc7_amd64_gcc900/external/cascade/2.2.04-llifpc2
GDB_BASE:=/cvmfs/cms.cern.ch/slc7_amd64_gcc900/external/gdb/10.2
CLHEP_BASE:=/cvmfs/cms.cern.ch/slc7_amd64_gcc900/external/clhep/2.4.4.0-llifpc2
PY3_GOOGLEPACKAGES_BASE:=/cvmfs/cms.cern.ch/slc7_amd64_gcc900/external/py3-googlePackages/1.0-llifpc
FMT_BASE:=/cvmfs/cms.cern.ch/slc7_amd64_gcc900/external/fmt/7.0.1-llifpc2
PY3_FUTURE_BASE:=/cvmfs/cms.cern.ch/slc7_amd64_gcc900/external/py3-future/0.18.2-llifpc
LHAPDF_BASE:=/cvmfs/cms.cern.ch/slc7_amd64_gcc900/external/lhapdf/6.3.0
DAS_CLIENT_BASE:=/cvmfs/cms.cern.ch/slc7_amd64_gcc900/cms/das_client/v03.01.00-llifpc
KTJET_BASE:=/cvmfs/cms.cern.ch/slc7_amd64_gcc900/external/ktjet/1.06-llifpc2
PHOTOSPP_BASE:=/cvmfs/cms.cern.ch/slc7_amd64_gcc900/external/photospp/3.64-llifpc
PY3_NBFORMAT_BASE:=/cvmfs/cms.cern.ch/slc7_amd64_gcc900/external/py3-nbformat/5.1.2-llifpc4
PY3_NOTEBOOK_BASE:=/cvmfs/cms.cern.ch/slc7_amd64_gcc900/external/py3-notebook/6.2.0-llifpc4
ORACLE_BASE:=/cvmfs/cms.cern.ch/slc7_amd64_gcc900/external/oracle/19.11.0.0.0dbru
ORACLE_ADMINDIR:=/etc
PY3_NOSE_BASE:=/cvmfs/cms.cern.ch/slc7_amd64_gcc900/external/py3-nose/1.3.7-llifpc
DD4HEP_CORE_BASE:=/cvmfs/cms.cern.ch/slc7_amd64_gcc900/external/dd4hep/v01-18x-f0e33d73d30434b7e1f0c04df8566b9c
LLVM_ANALYZER_CXXCOMPILER_BASE:=/cvmfs/cms.cern.ch/slc7_amd64_gcc900/external/llvm/12.0.0-dba5ec8511dcec4cd8ee2b03d956f9be
ifeq ($(strip $(SCRAM_COMPILER)),llvm-analyzer)
CXX:=/cvmfs/cms.cern.ch/slc7_amd64_gcc900/external/llvm/12.0.0-dba5ec8511dcec4cd8ee2b03d956f9be/bin/c++-analyzer
endif
llvm-analyzer_CXX:=/cvmfs/cms.cern.ch/slc7_amd64_gcc900/external/llvm/12.0.0-dba5ec8511dcec4cd8ee2b03d956f9be/bin/c++-analyzer
PY3_PKGINFO_BASE:=/cvmfs/cms.cern.ch/slc7_amd64_gcc900/external/py3-pkginfo/1.7.0-llifpc
PY3_VIRTUALENV_BASE:=/cvmfs/cms.cern.ch/slc7_amd64_gcc900/external/py3-virtualenv/20.4.2-llifpc3
PY3_PYFLAKES_BASE:=/cvmfs/cms.cern.ch/slc7_amd64_gcc900/external/py3-pyflakes/2.2.0-llifpc
PY3_SYMPY_BASE:=/cvmfs/cms.cern.ch/slc7_amd64_gcc900/external/py3-sympy/1.7.1-llifpc3
PY3_SETUPTOOLS_BASE:=/cvmfs/cms.cern.ch/slc7_amd64_gcc900/external/py3-setuptools/51.3.3-llifpc
PY3_PYGMENTS_BASE:=/cvmfs/cms.cern.ch/slc7_amd64_gcc900/external/py3-Pygments/2.8.1-llifpc
PYDATA_BASE:=/cvmfs/cms.cern.ch/slc7_amd64_gcc900/external/pythia6/426-ghbfee
PY3_TENSORBOARD_BASE:=/cvmfs/cms.cern.ch/slc7_amd64_gcc900/external/py3-tensorboard/2.5.0
PYTHIA6_HEADERS_BASE:=/cvmfs/cms.cern.ch/slc7_amd64_gcc900/external/pythia6/426-ghbfee
PY3_WHEEL_BASE:=/cvmfs/cms.cern.ch/slc7_amd64_gcc900/external/py3-wheel/0.33.6-llifpc
ROOT_INTERFACE_BASE:=/cvmfs/cms.cern.ch/slc7_amd64_gcc900/lcg/root/6.22.08-44c56ac2ea72d4504197c078eb47fd23
PY3_PBR_BASE:=/cvmfs/cms.cern.ch/slc7_amd64_gcc900/external/py3-pbr/5.5.1-llifpc
PY3_PYTEST_BASE:=/cvmfs/cms.cern.ch/slc7_amd64_gcc900/external/py3-pytest/6.2.2-llifpc3
PY3_NBDIME_BASE:=/cvmfs/cms.cern.ch/slc7_amd64_gcc900/external/py3-nbdime/1.1.0-llifpc3
PYTHIA8_BASE:=/cvmfs/cms.cern.ch/slc7_amd64_gcc900/external/pythia8/306
PY3_ONNX_BASE:=/cvmfs/cms.cern.ch/slc7_amd64_gcc900/external/py3-onnx/1.8.1-llifpc
PYCLANG_BASE:=/cvmfs/cms.cern.ch/slc7_amd64_gcc900/external/llvm/12.0.0-dba5ec8511dcec4cd8ee2b03d956f9be
ROOFIT_BASE:=/cvmfs/cms.cern.ch/slc7_amd64_gcc900/lcg/root/6.22.08-44c56ac2ea72d4504197c078eb47fd23
SQLITE_BASE:=/cvmfs/cms.cern.ch/slc7_amd64_gcc900/external/sqlite/3.22.0-bcolbf
TCMALLOC_BASE:=/cvmfs/cms.cern.ch/slc7_amd64_gcc900/external/gperftools/2.9.1-cms
PY3_CMSML_BASE:=/cvmfs/cms.cern.ch/slc7_amd64_gcc900/external/py3-cmsml/0.1.1-llifpc2
PY3_HISTOPRINT_BASE:=/cvmfs/cms.cern.ch/slc7_amd64_gcc900/external/py3-histoprint/1.6.0-llifpc4
PY3_HIST_BASE:=/cvmfs/cms.cern.ch/slc7_amd64_gcc900/external/py3-hist/2.0.1-llifpc4
XGBOOST_BASE:=/cvmfs/cms.cern.ch/slc7_amd64_gcc900/external/xgboost/1.3.3-llifpc
TENSORFLOW_BASE:=/cvmfs/cms.cern.ch/slc7_amd64_gcc900/external/tensorflow/2.5.0-0f8ac5425ea43361cb3c15441383add7
SHERPA_BASE:=/cvmfs/cms.cern.ch/slc7_amd64_gcc900/external/sherpa/2.2.11-73da7726869ce2020deee8c413c03f8e
TAUOLA_BASE:=/cvmfs/cms.cern.ch/slc7_amd64_gcc900/external/tauola/27.121.5-ghbfee
YODA_BASE:=/cvmfs/cms.cern.ch/slc7_amd64_gcc900/external/yoda/1.9.0-8ee534fb00c3d2d0005e80b84913e396
XROOTD_BASE:=/cvmfs/cms.cern.ch/slc7_amd64_gcc900/external/xrootd/4.12.3-llifpc2
PY3_CHARDET_BASE:=/cvmfs/cms.cern.ch/slc7_amd64_gcc900/external/py3-chardet/3.0.4-llifpc
PY3_DOCUTILS_BASE:=/cvmfs/cms.cern.ch/slc7_amd64_gcc900/external/py3-docutils/0.16-llifpc2
ALPGEN_BASE:=/cvmfs/cms.cern.ch/slc7_amd64_gcc900/external/alpgen/214-bcolbf
GEANT4DATA_BASE:=/cvmfs/cms.cern.ch/slc7_amd64_gcc900/external
VDT_HEADERS_BASE:=/cvmfs/cms.cern.ch/slc7_amd64_gcc900/cms/vdt/0.4.0-llifpc
CASCADE_HEADERS_BASE:=/cvmfs/cms.cern.ch/slc7_amd64_gcc900/external/cascade/2.2.04-llifpc2
TKONLINESW_BASE:=/cvmfs/cms.cern.ch/slc7_amd64_gcc900/external/tkonlinesw/4.2.0-1_gcc7-ff1d0340fb9fd439f976e21e3042c964
LIBUNGIF_BASE:=/cvmfs/cms.cern.ch/slc7_amd64_gcc900/external/libungif/4.1.4-bcolbf2
VDT_BASE:=/cvmfs/cms.cern.ch/slc7_amd64_gcc900/cms/vdt/0.4.0-llifpc
DMTCP_BASE:=/cvmfs/cms.cern.ch/slc7_amd64_gcc900/external/dmtcp/3.0.0-dev-bcolbf
IGPROF_BASE:=/cvmfs/cms.cern.ch/slc7_amd64_gcc900/external/igprof/5.9.16-llifpc3
DIP_INTERFACE_BASE:=/cvmfs/cms.cern.ch/slc7_amd64_gcc900/external/dip/8693f00cc422b4a15858fcd84249acaeb07b6316-llifpc4
PY3_JSONSCHEMA_BASE:=/cvmfs/cms.cern.ch/slc7_amd64_gcc900/external/py3-jsonschema/3.2.0-llifpc2
PY3_IPYTHON_BASE:=/cvmfs/cms.cern.ch/slc7_amd64_gcc900/external/py3-ipython/7.22.0-llifpc3
PY3_BOKEH_BASE:=/cvmfs/cms.cern.ch/slc7_amd64_gcc900/external/py3-bokeh/2.3.0-llifpc3
HERWIGPP_BASE:=/cvmfs/cms.cern.ch/slc7_amd64_gcc900/external/herwig7/7.2.2-fe4a2419f4fed778626e0644b267fdaa
PY3_KEYRING_BASE:=/cvmfs/cms.cern.ch/slc7_amd64_gcc900/external/py3-keyring/22.3.0-llifpc2
LCOV_BASE:=/cvmfs/cms.cern.ch/slc7_amd64_gcc900/external/lcov/1.9-bcolbf
EXPAT_BASE:=/cvmfs/cms.cern.ch/slc7_amd64_gcc900/external/expat/2.1.0-bcolbf2
NVPERF_BASE:=/cvmfs/cms.cern.ch/slc7_amd64_gcc900/external/cuda/11.2.2-541fb61f7fe80980f444615bcda5231f
MILLEPEDE_BASE:=/cvmfs/cms.cern.ch/slc7_amd64_gcc900/external/millepede/V04-09-01
QD_BASE:=/cvmfs/cms.cern.ch/slc7_amd64_gcc900/external/qd/2.3.13-bcolbf2
MD5_BASE:=/cvmfs/cms.cern.ch/slc7_amd64_gcc900/external/md5/1.0.0-bcolbf2
DCAP_BASE:=/cvmfs/cms.cern.ch/slc7_amd64_gcc900/external/dcap/2.47.12-cms
GMP_BASE:=/cvmfs/cms.cern.ch/slc7_amd64_gcc900/external/gmp-static/6.2.0-cms
PY3_PYBIND11_BASE:=/cvmfs/cms.cern.ch/slc7_amd64_gcc900/external/py3-pybind11/2.6.2-llifpc2
CORAL_BASE:=/cvmfs/cms.cern.ch/slc7_amd64_gcc900/cms/coral/CORAL_2_3_21-ae548bc03c6614d2ee8f3d4946fba3fd
LLVM_ANALYZER_CCOMPILER_BASE:=/cvmfs/cms.cern.ch/slc7_amd64_gcc900/external/llvm/12.0.0-dba5ec8511dcec4cd8ee2b03d956f9be
ifeq ($(strip $(SCRAM_COMPILER)),llvm-analyzer)
CC:=/cvmfs/cms.cern.ch/slc7_amd64_gcc900/external/llvm/12.0.0-dba5ec8511dcec4cd8ee2b03d956f9be/bin/ccc-analyzer
endif
llvm-analyzer_CC:=/cvmfs/cms.cern.ch/slc7_amd64_gcc900/external/llvm/12.0.0-dba5ec8511dcec4cd8ee2b03d956f9be/bin/ccc-analyzer
HECTOR_BASE:=/cvmfs/cms.cern.ch/slc7_amd64_gcc900/external/hector/1.3.4_patch1-90cb96642cfe7b057080a48ecb6d0df0
PHOTOS_HEADERS_BASE:=/cvmfs/cms.cern.ch/slc7_amd64_gcc900/external/photos/215.5-bcolbf
ZLIB_BASE:=/cvmfs/cms.cern.ch/slc7_amd64_gcc900/external/zlib/1.2.11-bcolbf
PY3_FLIT_BASE:=/cvmfs/cms.cern.ch/slc7_amd64_gcc900/external/py3-flit/3.1.0-llifpc3
PROTOBUF_BASE:=/cvmfs/cms.cern.ch/slc7_amd64_gcc900/external/protobuf/3.15.1-llifpc2
PYQUEN_BASE:=/cvmfs/cms.cern.ch/slc7_amd64_gcc900/external/pyquen/1.5.4-llifpc2
PY3_THEANO_BASE:=/cvmfs/cms.cern.ch/slc7_amd64_gcc900/external/py3-Theano/1.0.5-llifpc2
PY3_POETRY_BASE:=/cvmfs/cms.cern.ch/slc7_amd64_gcc900/external/py3-poetry/1.1.4-llifpc4
PY3_NBCONVERT_BASE:=/cvmfs/cms.cern.ch/slc7_amd64_gcc900/external/py3-nbconvert/5.6.1-llifpc3
VECGEOM_BASE:=/cvmfs/cms.cern.ch/slc7_amd64_gcc900/external/vecgeom/v1.1.16-llifpc
PY3_PYLINT_BASE:=/cvmfs/cms.cern.ch/slc7_amd64_gcc900/external/py3-pylint/2.7.2-llifpc3
YAML_CPP_BASE:=/cvmfs/cms.cern.ch/slc7_amd64_gcc900/external/yaml-cpp/0.6.2-16b3600b5e8746dcbbc387701f1a2f10
TBBBIND_BASE:=/cvmfs/cms.cern.ch/slc7_amd64_gcc900/external/tbb/v2021.2.0-668f9dc16735894655a062ddf70e63db
PYTHIA6_BASE:=/cvmfs/cms.cern.ch/slc7_amd64_gcc900/external/pythia6/426-ghbfee
RIVET_BASE:=/cvmfs/cms.cern.ch/slc7_amd64_gcc900/external/rivet/3.1.4-30e7400b5b5ace87b930e60424cadc0b
PY3_AVRO_BASE:=/cvmfs/cms.cern.ch/slc7_amd64_gcc900/external/py3-avro/1.10.1-llifpc3
PY3_PLAC_BASE:=/cvmfs/cms.cern.ch/slc7_amd64_gcc900/external/py3-plac/1.3.2
PY3_NUMPY_BASE:=/cvmfs/cms.cern.ch/slc7_amd64_gcc900/external/py3-numpy/1.17.5-llifpc3
PY3_TQDM_BASE:=/cvmfs/cms.cern.ch/slc7_amd64_gcc900/external/py3-tqdm/4.51.0
PY3_ISORT_BASE:=/cvmfs/cms.cern.ch/slc7_amd64_gcc900/external/py3-isort/4.3.21-llifpc
PY3_TENSORFLOW_BASE:=/cvmfs/cms.cern.ch/slc7_amd64_gcc900/external/py3-tensorflow/2.5.0-410a882e155edb340a4e06472738065f
ROOTRFLX_BASE:=/cvmfs/cms.cern.ch/slc7_amd64_gcc900/lcg/root/6.22.08-44c56ac2ea72d4504197c078eb47fd23
TAUOLAPP_BASE:=/cvmfs/cms.cern.ch/slc7_amd64_gcc900/external/tauolapp/1.1.8-llifpc2
TINYXML2_BASE:=/cvmfs/cms.cern.ch/slc7_amd64_gcc900/external/tinyxml2/6.2.0-llifpc
SLOCCOUNT_BASE:=/cvmfs/cms.cern.ch/slc7_amd64_gcc900/external/sloccount/2.26-cms
XERCES_C_BASE:=/cvmfs/cms.cern.ch/slc7_amd64_gcc900/external/xerces-c/3.1.3-bcolbf2
VALGRIND_BASE:=/cvmfs/cms.cern.ch/slc7_amd64_gcc900/external/valgrind/3.15.0-cms
PY3_JUPYTER_CORE_BASE:=/cvmfs/cms.cern.ch/slc7_amd64_gcc900/external/py3-jupyter_core/4.6.3-llifpc3
SIGCPP_BASE:=/cvmfs/cms.cern.ch/slc7_amd64_gcc900/external/sigcpp/2.6.2-cms
THEPEG_BASE:=/cvmfs/cms.cern.ch/slc7_amd64_gcc900/external/thepeg/2.2.2-llifpc3
TRITON_INFERENCE_CLIENT_BASE:=/cvmfs/cms.cern.ch/slc7_amd64_gcc900/external/triton-inference-client/2.11.0-b16e1487927304720ab83d7f8b8a5d0f
TAUOLA_HEADERS_BASE:=/cvmfs/cms.cern.ch/slc7_amd64_gcc900/external/tauola/27.121.5-ghbfee
PY3_LUIGI_BASE:=/cvmfs/cms.cern.ch/slc7_amd64_gcc900/external/py3-luigi/3.0.2-llifpc4
PY3_VIRTUALENVWRAPPER_BASE:=/cvmfs/cms.cern.ch/slc7_amd64_gcc900/external/py3-virtualenvwrapper/4.8.4-llifpc3
PYTHON3_BASE:=/cvmfs/cms.cern.ch/slc7_amd64_gcc900/external/python3/3.9.6
PYTHON3_COMPILE:=/cvmfs/cms.cern.ch/slc7_amd64_gcc900/external/python3/3.9.6/lib/python3.9/compileall.py
TOPREX_BASE:=/cvmfs/cms.cern.ch/slc7_amd64_gcc900/external/toprex/4.23-ghbfee
############## All SCRAM ENV variables ################
SCRAM_ARCH:=slc7_amd64_gcc900
SCRAM_CXX11_ABI:=1
SCRAM_LOOKUPDB_WRITE:=/cvmfs/cms.cern.ch
SCRAM_PROJECTNAME:=CMSSW
SCRAM_PROJECTVERSION:=CMSSW_12_0_2
SCRAM_CONFIGDIR:=config
SCRAM_SOURCEDIR:=src
SCRAM_CONFIGCHKSUM:=V06-03-07
SCRAM_TMP:=tmp
RELEASETOP:=/cvmfs/cms.cern.ch/slc7_amd64_gcc900/cms/cmssw/CMSSW_12_0_2
SCRAM_INTwork:=tmp/slc7_amd64_gcc900
SCRAM_RUNTIME_TYPE:=BUILD
SCRAM_BUILDFILE:=BuildFile
SCRAM_INIT_LOCALTOP:=/afs/cern.ch/user/n/nmancill/HSCP/CMSSW_12_0_2
################ ALL SCRAM Stores #######################
ALL_PRODUCT_STORES:=
SCRAMSTORENAME_LIB:=lib/slc7_amd64_gcc900
ALL_PRODUCT_STORES+=$(SCRAMSTORENAME_LIB)
SCRAMSTORENAME_BIN:=bin/slc7_amd64_gcc900
ALL_PRODUCT_STORES+=$(SCRAMSTORENAME_BIN)
SCRAMSTORENAME_TEST:=test/slc7_amd64_gcc900
ALL_PRODUCT_STORES+=$(SCRAMSTORENAME_TEST)
SCRAMSTORENAME_LOGS:=logs/slc7_amd64_gcc900
ALL_PRODUCT_STORES+=$(SCRAMSTORENAME_LOGS)
SCRAMSTORENAME_OBJS:=objs/slc7_amd64_gcc900
ALL_PRODUCT_STORES+=$(SCRAMSTORENAME_OBJS)
SCRAMSTORENAME_BIGLIB:=biglib/slc7_amd64_gcc900
ALL_PRODUCT_STORES+=$(SCRAMSTORENAME_BIGLIB)
SCRAMSTORENAME_CFIPYTHON:=cfipython/slc7_amd64_gcc900
ALL_PRODUCT_STORES+=$(SCRAMSTORENAME_CFIPYTHON)
SCRAMSTORENAME_STATIC:=static/slc7_amd64_gcc900
ALL_PRODUCT_STORES+=$(SCRAMSTORENAME_STATIC)
SCRAMSTORENAME_INCLUDE:=include
ALL_PRODUCT_STORES+=$(SCRAMSTORENAME_INCLUDE)
SCRAMSTORENAME_DOC:=doc
ALL_PRODUCT_STORES+=$(SCRAMSTORENAME_DOC)
SCRAMSTORENAME_PYTHON:=python
ALL_PRODUCT_STORES+=$(SCRAMSTORENAME_PYTHON)
