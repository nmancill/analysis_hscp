ifeq ($(strip $(HSCPAnalysismuSimHitsAuto)),)
HSCPAnalysismuSimHitsAuto := self/src/HSCPAnalysis/muSimHits/plugins
PLUGINS:=yes
HSCPAnalysismuSimHitsAuto_files := $(patsubst src/HSCPAnalysis/muSimHits/plugins/%,%,$(wildcard $(foreach dir,src/HSCPAnalysis/muSimHits/plugins ,$(foreach ext,$(SRC_FILES_SUFFIXES),$(dir)/*.$(ext)))))
HSCPAnalysismuSimHitsAuto_BuildFile    := $(WORKINGDIR)/cache/bf/src/HSCPAnalysis/muSimHits/plugins/BuildFile
HSCPAnalysismuSimHitsAuto_LOC_USE := self   FWCore/Framework FWCore/PluginManager FWCore/ParameterSet DataFormats/DetId DataFormats/MuonDetId RecoMuon/DetLayers
HSCPAnalysismuSimHitsAuto_PRE_INIT_FUNC += $$(eval $$(call edmPlugin,HSCPAnalysismuSimHitsAuto,HSCPAnalysismuSimHitsAuto,$(SCRAMSTORENAME_LIB),src/HSCPAnalysis/muSimHits/plugins))
HSCPAnalysismuSimHitsAuto_PACKAGE := self/src/HSCPAnalysis/muSimHits/plugins
ALL_PRODS += HSCPAnalysismuSimHitsAuto
HSCPAnalysis/muSimHits_forbigobj+=HSCPAnalysismuSimHitsAuto
HSCPAnalysismuSimHitsAuto_INIT_FUNC        += $$(eval $$(call Library,HSCPAnalysismuSimHitsAuto,src/HSCPAnalysis/muSimHits/plugins,src_HSCPAnalysis_muSimHits_plugins,$(SCRAMSTORENAME_BIN),,$(SCRAMSTORENAME_LIB),$(SCRAMSTORENAME_LOGS),edm))
HSCPAnalysismuSimHitsAuto_CLASS := LIBRARY
else
$(eval $(call MultipleWarningMsg,HSCPAnalysismuSimHitsAuto,src/HSCPAnalysis/muSimHits/plugins))
endif
ALL_COMMONRULES += src_HSCPAnalysis_muSimHits_plugins
src_HSCPAnalysis_muSimHits_plugins_parent := HSCPAnalysis/muSimHits
src_HSCPAnalysis_muSimHits_plugins_INIT_FUNC += $$(eval $$(call CommonProductRules,src_HSCPAnalysis_muSimHits_plugins,src/HSCPAnalysis/muSimHits/plugins,PLUGINS))
