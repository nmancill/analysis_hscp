ifeq ($(strip $(HSCPAnalysisHSCPTriggerAuto)),)
HSCPAnalysisHSCPTriggerAuto := self/src/HSCPAnalysis/HSCPTrigger/plugins
PLUGINS:=yes
HSCPAnalysisHSCPTriggerAuto_files := $(patsubst src/HSCPAnalysis/HSCPTrigger/plugins/%,%,$(wildcard $(foreach dir,src/HSCPAnalysis/HSCPTrigger/plugins ,$(foreach ext,$(SRC_FILES_SUFFIXES),$(dir)/*.$(ext)))))
HSCPAnalysisHSCPTriggerAuto_BuildFile    := $(WORKINGDIR)/cache/bf/src/HSCPAnalysis/HSCPTrigger/plugins/BuildFile
HSCPAnalysisHSCPTriggerAuto_LOC_USE := self   FWCore/Framework FWCore/PluginManager FWCore/ParameterSet SimDataFormats/RPCDigiSimLink SimDataFormats/Vertex SimDataFormats/GeneratorProducts SimGeneral/HepPDTRecord DataFormats/DetId DataFormats/MuonDetId Geometry/CommonDetUnit RecoMuon/DetLayers rootcore rootgraphics root CommonTools/UtilAlgos DataFormats/RPCDigi Geometry/RPCGeometry
HSCPAnalysisHSCPTriggerAuto_PRE_INIT_FUNC += $$(eval $$(call edmPlugin,HSCPAnalysisHSCPTriggerAuto,HSCPAnalysisHSCPTriggerAuto,$(SCRAMSTORENAME_LIB),src/HSCPAnalysis/HSCPTrigger/plugins))
HSCPAnalysisHSCPTriggerAuto_PACKAGE := self/src/HSCPAnalysis/HSCPTrigger/plugins
ALL_PRODS += HSCPAnalysisHSCPTriggerAuto
HSCPAnalysis/HSCPTrigger_forbigobj+=HSCPAnalysisHSCPTriggerAuto
HSCPAnalysisHSCPTriggerAuto_INIT_FUNC        += $$(eval $$(call Library,HSCPAnalysisHSCPTriggerAuto,src/HSCPAnalysis/HSCPTrigger/plugins,src_HSCPAnalysis_HSCPTrigger_plugins,$(SCRAMSTORENAME_BIN),,$(SCRAMSTORENAME_LIB),$(SCRAMSTORENAME_LOGS),edm))
HSCPAnalysisHSCPTriggerAuto_CLASS := LIBRARY
else
$(eval $(call MultipleWarningMsg,HSCPAnalysisHSCPTriggerAuto,src/HSCPAnalysis/HSCPTrigger/plugins))
endif
ALL_COMMONRULES += src_HSCPAnalysis_HSCPTrigger_plugins
src_HSCPAnalysis_HSCPTrigger_plugins_parent := HSCPAnalysis/HSCPTrigger
src_HSCPAnalysis_HSCPTrigger_plugins_INIT_FUNC += $$(eval $$(call CommonProductRules,src_HSCPAnalysis_HSCPTrigger_plugins,src/HSCPAnalysis/HSCPTrigger/plugins,PLUGINS))
