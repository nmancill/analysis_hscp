ifeq ($(strip $(HSCPAnalysisHSCPDIGISAuto)),)
HSCPAnalysisHSCPDIGISAuto := self/src/HSCPAnalysis/HSCPDIGIS/plugins
PLUGINS:=yes
HSCPAnalysisHSCPDIGISAuto_files := $(patsubst src/HSCPAnalysis/HSCPDIGIS/plugins/%,%,$(wildcard $(foreach dir,src/HSCPAnalysis/HSCPDIGIS/plugins ,$(foreach ext,$(SRC_FILES_SUFFIXES),$(dir)/*.$(ext)))))
HSCPAnalysisHSCPDIGISAuto_BuildFile    := $(WORKINGDIR)/cache/bf/src/HSCPAnalysis/HSCPDIGIS/plugins/BuildFile
HSCPAnalysisHSCPDIGISAuto_LOC_USE := self   FWCore/Framework FWCore/PluginManager FWCore/ParameterSet SimDataFormats/RPCDigiSimLink DataFormats/DetId DataFormats/MuonDetId Geometry/CommonDetUnit RecoMuon/DetLayers rootcore rootgraphics root CommonTools/UtilAlgos DataFormats/RPCDigi Geometry/RPCGeometry
HSCPAnalysisHSCPDIGISAuto_PRE_INIT_FUNC += $$(eval $$(call edmPlugin,HSCPAnalysisHSCPDIGISAuto,HSCPAnalysisHSCPDIGISAuto,$(SCRAMSTORENAME_LIB),src/HSCPAnalysis/HSCPDIGIS/plugins))
HSCPAnalysisHSCPDIGISAuto_PACKAGE := self/src/HSCPAnalysis/HSCPDIGIS/plugins
ALL_PRODS += HSCPAnalysisHSCPDIGISAuto
HSCPAnalysis/HSCPDIGIS_forbigobj+=HSCPAnalysisHSCPDIGISAuto
HSCPAnalysisHSCPDIGISAuto_INIT_FUNC        += $$(eval $$(call Library,HSCPAnalysisHSCPDIGISAuto,src/HSCPAnalysis/HSCPDIGIS/plugins,src_HSCPAnalysis_HSCPDIGIS_plugins,$(SCRAMSTORENAME_BIN),,$(SCRAMSTORENAME_LIB),$(SCRAMSTORENAME_LOGS),edm))
HSCPAnalysisHSCPDIGISAuto_CLASS := LIBRARY
else
$(eval $(call MultipleWarningMsg,HSCPAnalysisHSCPDIGISAuto,src/HSCPAnalysis/HSCPDIGIS/plugins))
endif
ALL_COMMONRULES += src_HSCPAnalysis_HSCPDIGIS_plugins
src_HSCPAnalysis_HSCPDIGIS_plugins_parent := HSCPAnalysis/HSCPDIGIS
src_HSCPAnalysis_HSCPDIGIS_plugins_INIT_FUNC += $$(eval $$(call CommonProductRules,src_HSCPAnalysis_HSCPDIGIS_plugins,src/HSCPAnalysis/HSCPDIGIS/plugins,PLUGINS))
