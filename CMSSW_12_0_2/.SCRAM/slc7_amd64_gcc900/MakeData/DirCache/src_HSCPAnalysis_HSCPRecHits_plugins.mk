ifeq ($(strip $(HSCPAnalysisHSCPRecHitsAuto)),)
HSCPAnalysisHSCPRecHitsAuto := self/src/HSCPAnalysis/HSCPRecHits/plugins
PLUGINS:=yes
HSCPAnalysisHSCPRecHitsAuto_files := $(patsubst src/HSCPAnalysis/HSCPRecHits/plugins/%,%,$(wildcard $(foreach dir,src/HSCPAnalysis/HSCPRecHits/plugins ,$(foreach ext,$(SRC_FILES_SUFFIXES),$(dir)/*.$(ext)))))
HSCPAnalysisHSCPRecHitsAuto_BuildFile    := $(WORKINGDIR)/cache/bf/src/HSCPAnalysis/HSCPRecHits/plugins/BuildFile
HSCPAnalysisHSCPRecHitsAuto_LOC_USE := self   FWCore/Framework FWCore/PluginManager FWCore/ParameterSet SimDataFormats/RPCDigiSimLink SimDataFormats/Vertex SimDataFormats/GeneratorProducts SimGeneral/HepPDTRecord DataFormats/DetId DataFormats/MuonDetId Geometry/CommonDetUnit RecoMuon/DetLayers rootcore rootgraphics root CommonTools/UtilAlgos DataFormats/RPCDigi Geometry/RPCGeometry Geometry/Records
HSCPAnalysisHSCPRecHitsAuto_PRE_INIT_FUNC += $$(eval $$(call edmPlugin,HSCPAnalysisHSCPRecHitsAuto,HSCPAnalysisHSCPRecHitsAuto,$(SCRAMSTORENAME_LIB),src/HSCPAnalysis/HSCPRecHits/plugins))
HSCPAnalysisHSCPRecHitsAuto_PACKAGE := self/src/HSCPAnalysis/HSCPRecHits/plugins
ALL_PRODS += HSCPAnalysisHSCPRecHitsAuto
HSCPAnalysis/HSCPRecHits_forbigobj+=HSCPAnalysisHSCPRecHitsAuto
HSCPAnalysisHSCPRecHitsAuto_INIT_FUNC        += $$(eval $$(call Library,HSCPAnalysisHSCPRecHitsAuto,src/HSCPAnalysis/HSCPRecHits/plugins,src_HSCPAnalysis_HSCPRecHits_plugins,$(SCRAMSTORENAME_BIN),,$(SCRAMSTORENAME_LIB),$(SCRAMSTORENAME_LOGS),edm))
HSCPAnalysisHSCPRecHitsAuto_CLASS := LIBRARY
else
$(eval $(call MultipleWarningMsg,HSCPAnalysisHSCPRecHitsAuto,src/HSCPAnalysis/HSCPRecHits/plugins))
endif
ALL_COMMONRULES += src_HSCPAnalysis_HSCPRecHits_plugins
src_HSCPAnalysis_HSCPRecHits_plugins_parent := HSCPAnalysis/HSCPRecHits
src_HSCPAnalysis_HSCPRecHits_plugins_INIT_FUNC += $$(eval $$(call CommonProductRules,src_HSCPAnalysis_HSCPRecHits_plugins,src/HSCPAnalysis/HSCPRecHits/plugins,PLUGINS))
