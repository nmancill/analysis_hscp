ifeq ($(strip $(HSCPAnalysisHSCPSIMHitsAuto)),)
HSCPAnalysisHSCPSIMHitsAuto := self/src/HSCPAnalysis/HSCPSIMHits/plugins
PLUGINS:=yes
HSCPAnalysisHSCPSIMHitsAuto_files := $(patsubst src/HSCPAnalysis/HSCPSIMHits/plugins/%,%,$(wildcard $(foreach dir,src/HSCPAnalysis/HSCPSIMHits/plugins ,$(foreach ext,$(SRC_FILES_SUFFIXES),$(dir)/*.$(ext)))))
HSCPAnalysisHSCPSIMHitsAuto_BuildFile    := $(WORKINGDIR)/cache/bf/src/HSCPAnalysis/HSCPSIMHits/plugins/BuildFile
HSCPAnalysisHSCPSIMHitsAuto_LOC_USE := self   FWCore/Framework FWCore/PluginManager FWCore/ParameterSet SimDataFormats/GeneratorProducts DataFormats/DetId DataFormats/GeometrySurface DataFormats/GeometryVector DataFormats/MuonDetId Geometry/CommonDetUnit Geometry/RPCGeometry Geometry/Records RecoMuon/TrackingTools CommonTools/UtilAlgos
HSCPAnalysisHSCPSIMHitsAuto_PRE_INIT_FUNC += $$(eval $$(call edmPlugin,HSCPAnalysisHSCPSIMHitsAuto,HSCPAnalysisHSCPSIMHitsAuto,$(SCRAMSTORENAME_LIB),src/HSCPAnalysis/HSCPSIMHits/plugins))
HSCPAnalysisHSCPSIMHitsAuto_PACKAGE := self/src/HSCPAnalysis/HSCPSIMHits/plugins
ALL_PRODS += HSCPAnalysisHSCPSIMHitsAuto
HSCPAnalysis/HSCPSIMHits_forbigobj+=HSCPAnalysisHSCPSIMHitsAuto
HSCPAnalysisHSCPSIMHitsAuto_INIT_FUNC        += $$(eval $$(call Library,HSCPAnalysisHSCPSIMHitsAuto,src/HSCPAnalysis/HSCPSIMHits/plugins,src_HSCPAnalysis_HSCPSIMHits_plugins,$(SCRAMSTORENAME_BIN),,$(SCRAMSTORENAME_LIB),$(SCRAMSTORENAME_LOGS),edm))
HSCPAnalysisHSCPSIMHitsAuto_CLASS := LIBRARY
else
$(eval $(call MultipleWarningMsg,HSCPAnalysisHSCPSIMHitsAuto,src/HSCPAnalysis/HSCPSIMHits/plugins))
endif
ALL_COMMONRULES += src_HSCPAnalysis_HSCPSIMHits_plugins
src_HSCPAnalysis_HSCPSIMHits_plugins_parent := HSCPAnalysis/HSCPSIMHits
src_HSCPAnalysis_HSCPSIMHits_plugins_INIT_FUNC += $$(eval $$(call CommonProductRules,src_HSCPAnalysis_HSCPSIMHits_plugins,src/HSCPAnalysis/HSCPSIMHits/plugins,PLUGINS))
