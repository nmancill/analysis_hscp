ifeq ($(strip $(DataFormats/MuonDetId)),)
ALL_COMMONRULES += src_DataFormats_MuonDetId_src
src_DataFormats_MuonDetId_src_parent := DataFormats/MuonDetId
src_DataFormats_MuonDetId_src_INIT_FUNC := $$(eval $$(call CommonProductRules,src_DataFormats_MuonDetId_src,src/DataFormats/MuonDetId/src,LIBRARY))
DataFormatsMuonDetId := self/DataFormats/MuonDetId
DataFormats/MuonDetId := DataFormatsMuonDetId
DataFormatsMuonDetId_files := $(patsubst src/DataFormats/MuonDetId/src/%,%,$(wildcard $(foreach dir,src/DataFormats/MuonDetId/src ,$(foreach ext,$(SRC_FILES_SUFFIXES),$(dir)/*.$(ext)))))
DataFormatsMuonDetId_BuildFile    := $(WORKINGDIR)/cache/bf/src/DataFormats/MuonDetId/BuildFile
DataFormatsMuonDetId_LOC_USE := self   DataFormats/DetId FWCore/Utilities
DataFormatsMuonDetId_LCGDICTS  := x 
DataFormatsMuonDetId_PRE_INIT_FUNC += $$(eval $$(call LCGDict,DataFormatsMuonDetId,src/DataFormats/MuonDetId/src/classes.h,src/DataFormats/MuonDetId/src/classes_def.xml,$(SCRAMSTORENAME_LIB),$(GENREFLEX_ARGS) $(root_EX_FLAGS_GENREFLEX_FAILES_ON_WARNS)))
DataFormatsMuonDetId_EX_LIB   := DataFormatsMuonDetId
DataFormatsMuonDetId_EX_USE   := $(foreach d,$(DataFormatsMuonDetId_LOC_USE),$(if $($(d)_EX_FLAGS_NO_RECURSIVE_EXPORT),,$d))
DataFormatsMuonDetId_PACKAGE := self/src/DataFormats/MuonDetId/src
ALL_PRODS += DataFormatsMuonDetId
DataFormatsMuonDetId_CLASS := LIBRARY
DataFormats/MuonDetId_forbigobj+=DataFormatsMuonDetId
DataFormatsMuonDetId_INIT_FUNC        += $$(eval $$(call Library,DataFormatsMuonDetId,src/DataFormats/MuonDetId/src,src_DataFormats_MuonDetId_src,$(SCRAMSTORENAME_BIN),,$(SCRAMSTORENAME_LIB),$(SCRAMSTORENAME_LOGS),))
endif
